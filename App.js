import { AppLoading } from 'expo';
import { Asset } from 'expo-asset';
import * as Font from 'expo-font';
import React, { useState } from 'react';
import { Platform, StatusBar, StyleSheet, View,AppRegistry } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

import AppNavigator from './navigation/AppNavigator';

export default function App(props) {
  const [isLoadingComplete, setLoadingComplete] = useState(false);

  if (!isLoadingComplete && !props.skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={loadResourcesAsync}
        onError={handleLoadingError}
        onFinish={() => handleFinishLoading(setLoadingComplete)}
      />
    );
  } else {
    return (
      <View style={styles.container}>
        {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
        <AppNavigator />
      </View>
    );
  }
}

async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/robot-dev.png'),
      require('./assets/images/robot-prod.png'),
    ]),
    Font.loadAsync({
      // This is the font that we are using for our tab bar
      ...Ionicons.font,
      // We include SpaceMono because we use it in HomeScreen.js. Feel free to
      // remove this if you are not using it in your app
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      Advertising_Script_Monoline_Trial: require("./assets/fonts/Advertising_Script_Monoline_Trial.ttf"),
      Arial: require("./assets/fonts/Arial.ttf"),
      SohoGothicW04_Regular: require("./assets/fonts/SohoGothicW04-Regular.ttf"),
      Biloxi_Script: require("./assets/fonts/Biloxi_Script.ttf"),
      Calibri_Bold: require("./assets/fonts/Calibri_Bold.ttf"),
      Confetti_Stream: require("./assets/fonts/Confetti_Stream.ttf"),
      english_essay: require("./assets/fonts/english_essay.ttf"),
      haldanor: require("./assets/fonts/haldanor.ttf"),
      Jameel_Noori_Nastaleeq: require("./assets/fonts/Jameel_Noori_Nastaleeq.ttf"),
      Jameel_Noori_Nastaleeq_Kasheeda: require("./assets/fonts/Jameel_Noori_Nastaleeq_Kasheeda.ttf"),
      LateefRegOT: require("./assets/fonts/LateefRegOT.ttf"),
      LearningCurve: require("./assets/fonts/LearningCurve.otf"),
      SinkinSans: require("./assets/fonts/SinkinSans-100Thin.otf"),
      MintSpirit: require("./assets/fonts/MintSpiritNo2-Regular.otf"),
      ScheherazadeRegOT: require("./assets/fonts/ScheherazadeRegOT.ttf"),
      Signika_Regular: require("./assets/fonts/Signika-Regular.ttf"),
      Signika_Semibold: require("./assets/fonts/Signika-Semibold.ttf"),
      alvi_Nastaleeq_Lahori_shipped: require("./assets/fonts/alvi_Nastaleeq_Lahori_shipped.ttf"),
      Tahoma: require("./assets/fonts/Tahoma.ttf"),
      Times: require("./assets/fonts/times-new-roman.ttf"),
      TravelingTypewriter: require("./assets/fonts/TravelingTypewriter.ttf"),
      Sharmeen: require("./assets/fonts/new/Sharmeen.ttf"),
      Ulnoreenbld: require("./assets/fonts/new/Ulnoreenbld.ttf"),
      Urdu_Emad_Nastaliq: require("./assets/fonts/new/Urdu_Emad_Nastaliq.ttf"),
      Urdulife_italic: require("./assets/fonts/new/Urdulife_italic.ttf"),
      UrduLink: require("./assets/fonts/new/UrduLink.ttf"),
      Zafar: require("./assets/fonts/new/Zafar.ttf"),
      Zohra: require("./assets/fonts/new/Zohra.ttf"),
    }),
  ]);
}

function handleLoadingError(error) {
  // In this case, you might want to report the error to your error reporting
  // service, for example Sentry
  console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
  setLoadingComplete(true);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
AppRegistry.registerComponent('Textzoo Image Writer', () => App);