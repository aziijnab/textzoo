import React, { Component } from "react";
import { Header, Button, Left, Right, Icon, Container, Body, Title } from "native-base";
// import { inject } from "mobx-react/native";
// import IconBadge from "react-native-icon-badge";
import {
  Dimensions,
  Platform,
  StatusBar,
  View,
  Text
} from "react-native";

const { deviceHeight, deviceWidth } = Dimensions.get("window");
const statusBar_height = StatusBar.currentHeight; 
// @inject("routerActions")
class BaseHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
    this.navigate = this.props.navigation.navigate;
  }
  setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
  componentWillMount() {
    this.startHeaderHeight = 100;
    if (Platform.OS == "android") {
      this.startHeaderHeight = 50 + StatusBar.currentHeight;
    }
  }
  render() {
    const navigation = this.props.navigation;
    return (
      <View>
      <View style={{width:deviceWidth,height:50,backgroundColor:"#1c2631", marginTop: Platform.OS === 'ios' ? 30 : statusBar_height}}>
<View style={{flexDirection:"row"}}>
<View style={{width:"20%", height:"100%"}}>
<Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon style={{ color: "#fff" }} name="arrow-back" />
              </Button>
</View>
<View style={{width:"80%", height:"100%", alignContent:"center", alignItems:"center",justifyContent:"center"}}>
<View style={{paddingRight:"20%"}}>

{this.props.title ? (
  <Text style={{color:"#fff", fontWeight:"bold"}}>{this.props.title}</Text>

):null}
</View>
</View>
</View>
      </View>
        </View>
    );
  }
}


export default BaseHeader;
