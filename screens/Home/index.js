import React, { Component } from "react";
import {
  Text,
  View,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Keyboard,
  ImageBackground,
  CameraRoll,
  Platform,
  ScrollView,
  Share,
  Image,
  AsyncStorage,
  Alert
} from "react-native";
import * as ImagePicker from "expo-image-picker";
import Constants from "expo-constants";
import * as Permissions from "expo-permissions";
import ViewShot, { captureRef } from "react-native-view-shot";
import ActionSheet from "react-native-actionsheet";
import * as Sharing from "expo-sharing";
import Draggable from "react-native-draggable";
import {
  Container,
  Content,
  List,
  ListItem,
  Form,
  Icon,
  Segment,
  Item,
  Input,
  Drawer,
  Body,
  CheckBox,
  Button
} from "native-base";
import { fromHsv } from "react-native-color-picker";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;
export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      containWidth: 0,
      containHeight: 0,
      selectedIndex: 0,
      showDone_pannel: false,
      message: "",
      fontSize: 22,
      fontColor: "black",
      fontFamily: "Arial",
      message2: null,
      fontSize2: 22,
      fontColor2: "black",
      fontFamily2: "Arial",
      message3: null,
      fontSize3: 22,
      fontColor3: "black",
      fontFamily3: "Arial",
      backgroundColor: "transparent",
      bgImageSize: "cover",
      appBackground: 0,
      image:
        "https://backgrounddownload.com/wp-content/uploads/2018/09/1x1-white-background-3.jpg",
      text1: true,
      text2: false,
      text3: false,
      text1Outline: false,
      text2Outline: false,
      text3Outline: false,
      hidden: false,

      keyboardHeight: 0,
      keyboardSize: 0,
      showtab: true,
      showBar: true,
      tab: 6,
      focusedInput: 1,
      urlToSHare: "",
      first_x : 10,
                first_y : 10,
                 second_x : 10,
                second_y : 70,
                 third_x : 10,
                third_y : 130,
    };
    this.navigate = this.props.navigation.navigate;
  }
  componentDidMount() {
    this.getPermissionAsync();
    this.textInputField.focus();
  }
  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      "keyboardDidShow",
      this._keyboardDidShow
    );
    AsyncStorage.getItem("styles").then(styles_data => {
      const val = JSON.parse(styles_data);
      if (val) {
       console.log(val)
       AsyncStorage.clear();
          this.setState({
            selectedIndex: val.selectedIndex,
        fontSize:val.fontSize,
        fontColor:val.fontColor,
        fontSize2:val.fontSize2,
        fontColor2:val.fontColor2,
        fontSize3:val.fontSize3,
        fontColor3:val.fontColor3,
        // backgroundColor:val.backgroundColor,
        // appBackground:val.appBackground,
        // bgImageSize:val.bgImageSize,
        // image:val.image,
        text1Outline : val.text1Outline,
        text2Outline : val.text2Outline,
        text3Outline : val.text3Outline,
        containWidth : val.containWidth,
        containHeight: val.containHeight,
        hidden : val.hidden,
          })
      }
    });
  }
  text1Outline = () => {
    if (this.state.text1Outline) {
      this.setState({ text1Outline: false });
      this.state.text1Outline = false;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    } else {
      this.setState({ text1Outline: true });
      this.state.text1Outline = true;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  text2Outline = () => {
    if (this.state.text2Outline) {
      this.setState({ text2Outline: false });
      this.state.text2Outline = false;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    } else {
      this.setState({ text2Outline: true });
      this.state.text2Outline = true;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  text3Outline = () => {
    if (this.state.text3Outline) {
      this.setState({ text3Outline: false });
      this.state.text3Outline = false;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    } else {
      this.setState({ text3Outline: true });
      this.state.text3Outline = false;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      if (status !== "granted") {
      }
    }
  };
  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images
    });
    if (!result.cancelled) {
      Alert.alert(
        "Note",
        "Background Image size?",
        [
          {
            text: "Fit to Screen",
            onPress: () => {
              this.setState({
                appBackground: 0,
                image: result.uri,
                backgroundColor: "transparent"
              });
              this.state.appBackground = 0;
              this.setState({ bgImageSize: "cover" });
              this.state.image = result.uri;
              this.state.backgroundColor = "transparent";
              this.state.bgImageSize = "cover";
              AsyncStorage.setItem("styles", JSON.stringify(this.state));
            }
          },
          {
            text: "Original Size",
            onPress: () => {
              this.setState({
                appBackground: 0,
                image: result.uri,
                backgroundColor: "transparent"
              });
              this.state.appBackground = 0;
              this.setState({ bgImageSize: "contain" });
              this.state.image = result.uri;
              Image.getSize(result.uri, (width, height) => { 
                this.setState({containWidth:width,containHeight:height})
                this.state.backgroundColor = "transparent";
                this.state.bgImageSize = "contain";
                AsyncStorage.setItem("styles", JSON.stringify(this.state));
              });
              
              
             
            }
          },
          {
            text: "Stretch",
            onPress: () => {
              this.setState({
                appBackground: 0,
                image: result.uri,
                backgroundColor: "transparent"
              });
              this.state.appBackground = 0;
              this.setState({ bgImageSize: "stretch" });
              this.state.image = result.uri;
              
              this.state.backgroundColor = "transparent";
              this.state.bgImageSize = "stretch";
              AsyncStorage.setItem("styles", JSON.stringify(this.state));
            }
          }
        ],
        { cancelable: false }
      );
    }
  };
  handleIndexChange = index => {
    this.setState({
      selectedIndex: index
    });
  };
  increaseFontsize = () => {
    this.setState({ fontSize: this.state.fontSize + 4 });
    this.state.fontSize = this.state.fontSize + 4;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  decreaseFontsize = () => {
    if (this.state.fontSize > 0) {
      this.setState({ fontSize: this.state.fontSize - 4 });
      this.state.fontSize = this.state.fontSize - 4;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  increaseFontsize2 = () => {
    this.setState({ fontSize2: this.state.fontSize2 + 4 });
    this.state.fontSize2 = this.state.fontSize2 + 4;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  decreaseFontsize2 = () => {
    if (this.state.fontSize2 > 0) {
      this.setState({ fontSize2: this.state.fontSize2 - 4 });
      this.state.fontSize2 = this.state.fontSize2 - 4;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  increaseFontsize3 = () => {
    this.setState({ fontSize3: this.state.fontSize3 + 4 });
    this.state.fontSize3 = this.state.fontSize3 + 4;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  decreaseFontsize3 = () => {
    if (this.state.fontSize3 > 0) {
      this.setState({ fontSize3: this.state.fontSize3 - 4 });
      this.state.fontSize3 = this.state.fontSize3 - 4;
      AsyncStorage.setItem("styles", JSON.stringify(this.state));
    }
  };
  changeFontColor = color => {
    var hex = fromHsv(color);
    this.setState({ fontColor: hex });
    this.state.fontColor = hex;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeFontColor2 = color => {
    var hex2 = fromHsv(color);
    this.setState({ fontColor2: hex2 });
    this.state.fontColor2 = hex2;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeFontColor3 = color => {
    var hex3 = fromHsv(color);
    this.setState({ fontColor3: hex3 });
    this.state.fontColor3 = hex3;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeFontFamily = family => {
    var fontF = family;
    let reg = /^[a-zA-Z0-9 @"(\r|\n)*.*?><;,{}[\]\-_+=!@#$%\^&*|'(.+)]+$/;
    if (Platform.OS == "ios") {
      if (reg.test(this.state.message) === false) {
        if (family == "Advertising_Script_Monoline_Trial") {
          fontF = "CourierNewPSMT";
        }
        if (family == "Biloxi_Script") {
          fontF = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF = "Zafar";
        }
        if (family == "haldanor") {
          fontF = "Zohra";
        }
        if (family == "LearningCurve") {
          fontF = "Mishafi";
        }
        if (family == "SohoGothicW04_Regular") {
          fontF = "Times New Roman";
        }
        if (family == "MintSpirit") {
          fontF = "TimesNewRomanPSMT";
        }
        if (family == "Signika_Regular") {
          fontF = "DamascusLight";
        }
        if (family == "Signika_Semibold") {
          fontF = "DamascusMedium";
        }
        if (family == "Times") {
          fontF = "Farah";
        }
        if (family == "TravelingTypewriter") {
          fontF = "DamascusSemiBold";
        }
      }
    } else {
      if (reg.test(this.state.message) === false) {
        if (family == "Biloxi_Script") {
          fontF = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF = "Zafar";
        }
        if (family == "haldanor") {
          fontF = "Zohra";
        }
      }
    }
    this.setState({ fontFamily: fontF });
    this.state.fontFamily = fontF;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeFontFamily2 = family => {
    var fontF2 = family;
    let reg = /^[a-zA-Z0-9 @"(\r|\n)*.*?><;,{}[\]\-_+=!@#$%\^&*|'(.+)]+$/;

    if (Platform.OS == "ios") {
      if (reg.test(this.state.message2) === false) {
        if (family == "Advertising_Script_Monoline_Trial") {
          fontF2 = "CourierNewPSMT";
        }
        if (family == "Biloxi_Script") {
          fontF2 = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF2 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF2 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF2 = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF2 = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF2 = "Zafar";
        }
        if (family == "haldanor") {
          fontF2 = "Zohra";
        }
        if (family == "LearningCurve") {
          fontF2 = "Mishafi";
        }
        if (family == "SohoGothicW04_Regular") {
          fontF2 = "Times New Roman";
        }
        if (family == "MintSpirit") {
          fontF2 = "TimesNewRomanPSMT";
        }
        if (family == "Signika_Regular") {
          fontF2 = "DamascusLight";
        }
        if (family == "Signika_Semibold") {
          fontF2 = "DamascusMedium";
        }
        if (family == "Times") {
          fontF2 = "Farah";
        }
        if (family == "TravelingTypewriter") {
          fontF2 = "DamascusSemiBold";
        }
      }
    } else {
      if (reg.test(this.state.message2) === false) {
        if (family == "Biloxi_Script") {
          fontF2 = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF2 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF2 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF2 = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF2 = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF2 = "Zafar";
        }
        if (family == "haldanor") {
          fontF2 = "Zohra";
        }
      }
    }
    this.setState({ fontFamily2: fontF2 });
    this.state.fontFamily2 = fontF2;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeFontFamily3 = family => {
    var fontF3 = family;
    let reg = /^[a-zA-Z0-9 @"(\r|\n)*.*?><;,{}[\]\-_+=!@#$%\^&*|'(.+)]+$/;
    if (Platform.OS == "ios") {
      if (reg.test(this.state.message3) === false) {
        if (family == "Advertising_Script_Monoline_Trial") {
          fontF3 = "CourierNewPSMT";
        }
        if (family == "Biloxi_Script") {
          fontF3 = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF3 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF3 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF3 = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF3 = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF3 = "Zafar";
        }
        if (family == "haldanor") {
          fontF3 = "Zohra";
        }
        if (family == "LearningCurve") {
          fontF3 = "Mishafi";
        }
        if (family == "SohoGothicW04_Regular") {
          fontF3 = "Times New Roman";
        }
        if (family == "MintSpirit") {
          fontF3 = "TimesNewRomanPSMT";
        }
        if (family == "Signika_Regular") {
          fontF3 = "DamascusLight";
        }
        if (family == "Signika_Semibold") {
          fontF3 = "DamascusMedium";
        }
        if (family == "Times") {
          fontF3 = "Farah";
        }
        if (family == "TravelingTypewriter") {
          fontF3 = "DamascusSemiBold";
        }
      }
    } else {
      if (reg.test(this.state.message3) === false) {
        if (family == "Biloxi_Script") {
          fontF3 = "Sharmeen";
        }
        if (family == "Calibri_Bold") {
          fontF3 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Calibri_Bold") {
          fontF3 = "Urdu_Emad_Nastaliq";
        }
        if (family == "Confetti_Stream") {
          fontF3 = "Urdulife_italic";
        }
        if (family == "english_essay") {
          fontF3 = "UrduLink";
        }
        if (family == "SinkinSans") {
          fontF3 = "Zafar";
        }
        if (family == "haldanor") {
          fontF3 = "Zohra";
        }
      }
    }

    this.setState({ fontFamily3: fontF3 });
    this.state.fontFamily3 = fontF3;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };

  changeBackgroundColor = color => {
    this.setState({
      backgroundColor: color,
      appBackground: 0,
      image:
        "https://backgrounddownload.com/wp-content/uploads/2018/09/1x1-white-background-3.jpg"
    });
    this.state.backgroundColor = color;
    this.state.appBackground = 0;
    this.state.image =
      "https://backgrounddownload.com/wp-content/uploads/2018/09/1x1-white-background-3.jpg";
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  changeBackgroundImage = index => {
    this.setState({ backgroundColor: "transparent", appBackground: index });
    this.state.backgroundColor = "transparent";
    this.state.appBackground = index;
    AsyncStorage.setItem("styles", JSON.stringify(this.state));
  };
  captureImage = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    if (status !== "granted") {
      alert("Sorry, we need camera roll permissions to make this work!");
    } else {
      this.setState({
        hidden: true,
        showtab: false,
        showBar: false,
        showDone_pannel: false
      });

      this.refs.viewShot.capture().then(uri => {
        CameraRoll.saveToCameraRoll(uri, "photo");
        this.setState({
          hidden: false,
          showBar: true
        });
        Alert.alert("Success", "Image Saved to Gallery", [{ text: "OK" }], {
          cancelable: true
        });
      });
    }
  };
  showActionSheet = () => {
    this.ActionSheet.show();
  };
  showTextActionSheet = () => {
    this.TextActionSheet.show();
  };
  showText2ActionSheet = () => {
    this.Text2ActionSheet.show();
  };
  showText3ActionSheet = () => {
    this.Text3ActionSheet.show();
  };
  addText = () => {
    if (
      this.state.text1 == false &&
      this.state.text2 == false &&
      this.state.text3 == false
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text1: true, focusedInput: 1, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == false &&
      this.state.text2 == false &&
      this.state.text3 == true
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text1: true, focusedInput: 1, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == false &&
      this.state.text2 == true &&
      this.state.text3 == false
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text1: true, focusedInput: 1, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == false &&
      this.state.text2 == true &&
      this.state.text3 == true
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text1: true, focusedInput: 1, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == true &&
      this.state.text2 == false &&
      this.state.text3 == false
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text2: true, focusedInput: 2, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == true &&
      this.state.text2 == false &&
      this.state.text3 == true
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text2: true, focusedInput: 2, showtab: true, tab: 1 });
    } else if (
      this.state.text1 == true &&
      this.state.text2 == true &&
      this.state.text3 == false
    ) {
      if (this.state.tab == 6) {
        Keyboard.dismiss();
      }
      this.setState({ text3: true, focusedInput: 3, showtab: true, tab: 1 });
    } else {
      alert("Space Full. Delete Some text to add more.");
    }
  };

  onShare = async () => {
    Keyboard.dismiss();
    this.setState({
      hidden: true,
      showtab: false,
      showBar: false,
      showDone_pannel: false
    });
    this.state.tab = 1;
    this.refs.viewShot.capture().then(uri => {
      if (Platform.OS == "ios") {
        this.iosShare(uri);
      } else {
        this.share(uri);
      }

      this.setState({
        hidden: false,
        showBar: true
      });
    });
  };
  iosShare = async uri => {
    try {
      const result = await Share.share({
        url: uri
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
        } else {
        }
      } else if (result.action === Share.dismissedAction) {
      }
    } catch (error) {
      alert(error.message);
    }
  };
  share = async uri => {
    const { status } = await Sharing.shareAsync(uri, {
      mimeType: "Image/png",
      UTI: "Image/png"
    });
  };
  focusText1 = () => {
    this.showTextActionSheet();
  };
  _keyboardDidShow = e => {
    var KeyboardHeight = e.endCoordinates.height;
    this.setState({
      keyboardHeight: KeyboardHeight,
      keyboardSize: KeyboardHeight
    });
  };
  changeText1 = message => {
    this.setState({ message: message });
  };
  changeText2 = message => {
    this.setState({ message2: message });
  };
  changeText3 = message => {
    this.setState({ message3: message });
  };

  render() {
    var ImageUri = this.state.image;
    var navigation = this.props.navigation;
    // let eng_reg = /[-~]*$/;
    let eng_reg = /^[a-zA-Z0-9\/ @"(\r|\n)*.*?><;:,{}/\[\]\-_+=!@#$%\^&*|'(.+)]+$/;
    let toAlign = "left";
    let toAlign2 = "left";
    let toAlign3 = "left";
    if (eng_reg.test(this.state.message) === true) {
    //  console.log("first");
      toAlign = "left";
    } else if (this.state.message == "") {

      toAlign = "left";
    } else if (eng_reg.test(this.state.message) === false) {
     // console.log("second");

      toAlign = "right";
    } else {
      // console.log("forth");

      toAlign = "right";
    }
    if (eng_reg.test(this.state.message2) === true) {
      toAlign2 = "left";
    } else if (this.state.message2 == "") {
      // console.log("third");

      toAlign2 = "left";
    } else if (eng_reg.test(this.state.message2) === false) {
      toAlign2 = "right";
    } else {
      toAlign2 = "right";
    }
    if (eng_reg.test(this.state.message3) === true) {
      toAlign3 = "left";
    } else if (this.state.message3 == "") {
      // console.log("third");

      toAlign3 = "left";
    } else if (eng_reg.test(this.state.message3) === false) {
      toAlign3 = "right";
    } else {
      toAlign3 = "right";
    }
    console.log("focusedInput == > ")
    console.log(this.state.focusedInput)
    return (
      <Container>
        {Platform.OS == "android" ? (
          <Content>
            <View style={{ width: "100%", height: deviceHeight }}>
            <ViewShot
            style={{ width: "100%", height: this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? 250 : deviceHeight - 80 : deviceHeight, padding:0, margin:0  }}
            ref="viewShot"
            options={{ format: "png", quality: 0.9 }}
          >
             
            <ImageBackground
              source={{ uri: ImageUri }}
              style={{
                width:  deviceWidth,
                height:  this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? 250 : deviceHeight - 80 : deviceHeight
              }}
              imageStyle={
                !ImageUri
                  ? { resizeMode: "stretch" }
                  : {
                      resizeMode: this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? null : null : this.state.bgImageSize
                    }
              }
            >
                  <View
                    style={{
                      width: "100%",
                      height: deviceHeight,
                      backgroundColor: this.state.backgroundColor
                    }}
                  >
                   
                      {this.state.text1 ? (
                        <Draggable
                          reverse={false}
                          x={this.state.first_x}
                          y={this.state.first_y}
                          pressInDrag={() =>
                            Platform.OS == "android" ? this.focusText1() : null
                          }
                          pressDragRelease={() =>
                            Platform.OS == "android"
                              ? this.textInputField.focus()
                              : null
                          }
                        >
                          <Form
                            style={{ 
                              width: deviceWidth - 20,
                               paddingTop: 30 }}
                          >
                            {!this.state.hidden ? (
                              Platform.OS == "android" ? (
                                <TouchableOpacity
                                  onPress={() => this.showTextActionSheet()}
                                  style={{marginLeft :toAlign == "right" ? "auto" : null}}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 15,
                                        height: 15,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              ) : Platform.OS == "ios" ? (
                                <TouchableOpacity
                                  onPress={() => this.showTextActionSheet()}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 30,
                                        height: 30,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              ) : null
                            ) : null}
                            <TextInput
                              ref={ref => {
                                this.textInputField = ref;
                              }}
                              style={{
                                width: "100%",
                                fontSize: this.state.fontSize,
                                color: this.state.fontColor,
                                fontFamily: this.state.fontFamily,
                                fontWeight: "normal",
                                minHeight: 50,
                                zIndex: -1,
                                textShadowRadius: this.state.text1Outline
                                  ? 10
                                  : 0,
                                textShadowOffset: { width: 1, height: 1 },
                                textShadowColor: "#686F74"
                              }}
                              placeholder={!this.state.hidden ? "Enter text here" : ""}
                              value={this.state.message}
                              onChangeText={message =>
                                this.setState({ message })
                              }
                              onFocus={() =>
                                this.setState({
                                  focusedInput: 1,
                                  showtab: true,
                                  tab: 6
                                })
                              }
                              multiline={true}
                              maxLength={953}
                              enablesReturnKeyAutomatically={true}
                            />
                          </Form>
                        </Draggable>
                      ) : null}
                      {this.state.text2 ? (
                        <Draggable
                          reverse={false}
                          x={this.state.second_x}
                          y={this.state.second_y}
                          pressInDrag={() => this.showText2ActionSheet()}
                          pressDragRelease={() =>
                            Platform.OS == "android"
                              ? this.text2InputField.focus()
                              : null
                          }
                        >
                          <Form
                            style={{
                              width: deviceWidth - 20,
                              paddingVertical: 30
                            }}
                          >
                            {!this.state.hidden ? (
                              Platform.OS == "android" ? (
                                <TouchableOpacity
                                  onPress={() => this.showText2ActionSheet()}
                                  style={{marginLeft :toAlign2 == "right" ? "auto" : null}}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 15,
                                        height: 15,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              ) : (
                                <TouchableOpacity
                                  onPress={() => this.showText2ActionSheet()}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 25,
                                        height: 25,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              )
                            ) : null}
                            <TextInput
                              ref={ref => {
                                this.text2InputField = ref;
                              }}
                              onFocus={() =>
                                this.setState({
                                  focusedInput: 2,
                                  showtab: true,
                                  tab: 6
                                })
                              }
                              style={{
                                width: "100%",
                                fontSize: this.state.fontSize2,
                                color: this.state.fontColor2,
                                fontFamily: this.state.fontFamily2,
                                fontWeight: "normal",
                                zIndex: -1,
                                textShadowRadius: this.state.text2Outline
                                  ? 10
                                  : 0,
                                textShadowOffset: { width: 1, height: 1 },
                                textShadowColor: "#686F74"
                              }}
                              placeholder={!this.state.hidden ? "Enter text here" : ""}
                              value={this.state.message2}
                              onChangeText={message2 =>
                                this.setState({ message2 })
                              }
                              multiline={true}
                              maxLength={953}
                            />
                          </Form>
                        </Draggable>
                      ) : null}
                      {this.state.text3 ? (
                        <Draggable
                          reverse={false}
                          x={this.state.third_x}
                          y={this.state.third_y}
                          pressInDrag={() => this.showText3ActionSheet()}
                          pressDragRelease={() =>
                            Platform.OS == "android"
                              ? this.text3InputField.focus()
                              : null
                          }
                        >
                          <Form
                            style={{
                              width: deviceWidth - 20,
                              paddingVertical: 30
                            }}
                          >
                            {!this.state.hidden ? (
                              Platform.OS == "android" ? (
                                <TouchableOpacity
                                  onPress={() => this.showText3ActionSheet()}
                                  style={{marginLeft :toAlign3 == "right" ? "auto" : null}}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 15,
                                        height: 15,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              ) : (
                                <TouchableOpacity
                                  onPress={() => this.showText3ActionSheet()}
                                >
                                  <View>
                                    <Image
                                      style={{
                                        width: 25,
                                        height: 25,
                                        resizeMode: "contain"
                                      }}
                                      source={require("../../assets/images/edit-pen-icon.png")}
                                    />
                                  </View>
                                </TouchableOpacity>
                              )
                            ) : null}
                            <TextInput
                              ref={ref => {
                                this.text3InputField = ref;
                              }}
                              onFocus={() =>
                                this.setState({
                                  focusedInput: 3,
                                  showtab: true,
                                  tab: 6
                                })
                              }
                              style={{
                                width: "100%",
                                fontSize: this.state.fontSize3,
                                color: this.state.fontColor3,
                                fontFamily: this.state.fontFamily3,
                                fontWeight: "normal",
                                zIndex: -1,
                                textShadowRadius: this.state.text3Outline
                                  ? 10
                                  : 0,
                                textShadowOffset: { width: 1, height: 1 },
                                textShadowColor: "#686F74"
                              }}
                              placeholder={!this.state.hidden ? "Enter text here" : ""}
                              value={this.state.message3}
                              onChangeText={message3 =>
                                this.setState({ message3 })
                              }
                              multiline={true}
                              maxLength={953}
                            />
                          </Form>
                        </Draggable>
                      ) : null}
                      <View
                    style={ this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? {
                      position: "absolute",
                      top: 160,
                      right: 0,
                      zIndex: 10
                    } : {
                      position: "absolute",
                      bottom: 100,
                      right: 0,
                      zIndex: 10
                    } : {
                      position: "absolute",
                      bottom: 100,
                      right: 0,
                      zIndex: 10
                    } 
                      }
                  >
                    <Image
                      style={ this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ?
                        { height: 75, width: 75, resizeMode: "contain" }
                        :
                        { height: 100, width: 100, resizeMode: "contain" }:
                        { height: 100, width: 100, resizeMode: "contain" }}
                      source={require("../../assets/images/watermark.png")}
                    />
                  </View>

                    
                    
                  </View>
                </ImageBackground>
              </ViewShot>
              {this.state.showtab ? (
                        <View
                          style={{
                            width: deviceWidth,
                            height: this.state.keyboardSize,
                            backgroundColor: "#fff",
                            position: "absolute",
                            bottom: 0,
                            zIndex: 9999
                          }}
                        >
                          {this.state.tab == 1 ? (
                            <View
                              style={{
                                width: "100%",
                                height: this.state.keyboardSize,
                                paddingBottom: 40
                              }}
                            >
                              <ScrollView style={{ paddingBottom: 40 }}>
                                <View
                                  style={{
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center"
                                  }}
                                >
                                  <View
                                    style={{ width: "50%", marginVertical: 20 }}
                                  >
                                    <Button
                                      primary
                                      style={{
                                        alignItems: "center",
                                        alignContent: "center",
                                        justifyContent: "center"
                                      }}
                                      onPress={this._pickImage}
                                    >
                                      <Text
                                        style={{
                                          color: "#fff",
                                          fontSize: 16,
                                          fontWeight: "bold"
                                        }}
                                      >
                                        Browse
                                      </Text>
                                    </Button>
                                  </View>
                                  <View
                                    style={{
                                      flexDirection: "row",
                                      flexWrap: "wrap"
                                    }}
                                  >
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff3300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff3300"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff3300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffffff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffffff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffffff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#000000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#000000"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#000000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#009933")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009933"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#009933" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#6600ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#6600ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#6600ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff9900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff9900"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff9900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#663300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#663300"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#663300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffff80")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffff80"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffff80" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff99ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff99ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff99ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#b3ccff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#b3ccff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#b3ccff" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ccffcc")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ccffcc"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ccffcc" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff0000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff0000"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff0000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#808080")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#808080"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#808080" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#e6e6e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6e6e6"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#e6e6e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#f2f2f2")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#f2f2f2"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#f2f2f2" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#e6eeff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6eeff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#e6eeff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#993300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#993300"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#993300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#990099")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#990099"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#990099" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#0066ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#0066ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#0066ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#e6f0ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6f0ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#e6f0ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#003d99")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#003d99"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#003d99" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#66a3ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#66a3ff"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#66a3ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#00ff00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#00ff00"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#00ff00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#009900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009900"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#009900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#006600")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#006600"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#006600" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#4d4d4d")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#4d4d4d"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#4d4d4d" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#fff5e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#fff5e6"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#fff5e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#ffc266")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffc266"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#ffc266" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeBackgroundColor("#995c00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#995c00"
                                        }}
                                      >
                                        {this.state.backgroundColor ==
                                        "#995c00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </View>
                              </ScrollView>
                            </View>
                          ) : null}
                          {this.state.tab == 2 ? (
                            this.state.focusedInput == 1 ? (
                              <View style={{ width: "100%", marginTop: 10 }}>
                                <View style={{ flexDirection: "row" }}>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("Arial")
                                          }
                                          style={
                                            this.state.fontFamily == "Arial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Arial" }}>
                                            Arial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Advertising_Script_Monoline_Trial"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "CourierNewPSMT" ||
                                            this.state.fontFamily ==
                                              "Advertising_Script_Monoline_Trial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "Advertising_Script_Monoline_Trial"
                                            }}
                                          >
                                            Advertising Script Monoline Trial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Biloxi_Script"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Sharmeen" ||
                                            this.state.fontFamily ==
                                              "Biloxi_Script"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Biloxi_Script"
                                            }}
                                          >
                                            Biloxi Script
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("LateefRegOT")
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Ulnoreenbld" ||
                                            this.state.fontFamily ==
                                              "LateefRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LateefRegOT"
                                            }}
                                          >
                                            LateefRegOT
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Calibri_Bold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Urdu_Emad_Nastaliq" ||
                                            this.state.fontFamily ==
                                              "Calibri_Bold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Calibri_Bold"
                                            }}
                                          >
                                            Calibri Bold
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Confetti_Stream"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Urdulife_italic" ||
                                            this.state.fontFamily ==
                                              "Confetti_Stream"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Confetti_Stream"
                                            }}
                                          >
                                            Confetti Stream
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "english_essay"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "UrduLink" ||
                                            this.state.fontFamily ==
                                              "english_essay"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "english_essay"
                                            }}
                                          >
                                            English essay
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("SinkinSans")
                                          }
                                          style={
                                            this.state.fontFamily == "Zafar" ||
                                            this.state.fontFamily ==
                                              "SinkinSans"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "SinkinSans" }}
                                          >
                                            Sinkin Sans
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("haldanor")
                                          }
                                          style={
                                            this.state.fontFamily == "Zohra" ||
                                            this.state.fontFamily == "haldanor"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "haldanor" }}
                                          >
                                            Haldanor
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "alvi_Nastaleeq_Lahori_shipped"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                            "alvi_Nastaleeq_Lahori_shipped"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "alvi_Nastaleeq_Lahori_shipped",
                                            fontSize:14
                                                
                                            }}
                                          >
                                            Jameel Noori Nastaleeq
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "LearningCurve"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Mishafi" ||
                                            this.state.fontFamily ==
                                              "LearningCurve"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LearningCurve"
                                            }}
                                          >
                                            LearningCurve
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "SohoGothicW04_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "Times New Roman" ||
                                            this.state.fontFamily ==
                                              "SohoGothicW04_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "SohoGothicW04_Regular"
                                            }}
                                          >
                                            Soho
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "ScheherazadeRegOT"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                            "ScheherazadeRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "ScheherazadeRegOT"
                                            }}
                                          >
                                            ScheherazadeRegOT
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("MintSpirit")
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "TimesNewRomanPSMT" ||
                                            this.state.fontFamily ==
                                              "MintSpirit"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "MintSpirit" }}
                                          >
                                            Mint Spirit
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Signika_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "DamascusLight" ||
                                            this.state.fontFamily ==
                                              "Signika_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Regular"
                                            }}
                                          >
                                            Signika Regular
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "Signika_Semibold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "DamascusMedium" ||
                                            this.state.fontFamily ==
                                              "Signika_Semibold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Semibold"
                                            }}
                                          >
                                            Signika Semibold
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("Tahoma")
                                          }
                                          style={
                                            this.state.fontFamily == "Tahoma"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "Tahoma" }}
                                          >
                                            Tahoma
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily("Times")
                                          }
                                          style={
                                            this.state.fontFamily == "Farah" ||
                                            this.state.fontFamily == "Times"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Times" }}>
                                            Times
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily(
                                              "TravelingTypewriter"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily ==
                                              "DamascusSemiBold" ||
                                            this.state.fontFamily ==
                                              "TravelingTypewriter"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "TravelingTypewriter"
                                            }}
                                          >
                                            Traveling Typewriter
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                </View>
                              </View>
                            ) : this.state.focusedInput == 2 ? (
                              <View style={{ width: "100%", marginTop: 10 }}>
                                <View style={{ flexDirection: "row" }}>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("Arial")
                                          }
                                          style={
                                            this.state.fontFamily2 == "Arial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Arial" }}>
                                            Arial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Advertising_Script_Monoline_Trial"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "CourierNewPSMT" ||
                                            this.state.fontFamily2 ==
                                              "Advertising_Script_Monoline_Trial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "Advertising_Script_Monoline_Trial"
                                            }}
                                          >
                                            Advertising Script Monoline Trial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Biloxi_Script"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Sharmeen" ||
                                            this.state.fontFamily2 ==
                                              "Biloxi_Script"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Biloxi_Script"
                                            }}
                                          >
                                            Biloxi Script
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "LateefRegOT"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Ulnoreenbld" ||
                                            this.state.fontFamily2 ==
                                              "LateefRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LateefRegOT"
                                            }}
                                          >
                                            LateefRegOT
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Calibri_Bold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Urdu_Emad_Nastaliq" ||
                                            this.state.fontFamily2 ==
                                              "Calibri_Bold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Calibri_Bold"
                                            }}
                                          >
                                            Calibri Bold
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Confetti_Stream"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Urdulife_italic" ||
                                            this.state.fontFamily2 ==
                                              "Confetti_Stream"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Confetti_Stream"
                                            }}
                                          >
                                            Confetti Stream
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "english_essay"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "UrduLink" ||
                                            this.state.fontFamily2 ==
                                              "english_essay"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "english_essay"
                                            }}
                                          >
                                            English essay
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("SinkinSans")
                                          }
                                          style={
                                            this.state.fontFamily2 == "Zafar" ||
                                            this.state.fontFamily2 ==
                                              "SinkinSans"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "SinkinSans" }}
                                          >
                                            Sinkin Sans
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("haldanor")
                                          }
                                          style={
                                            this.state.fontFamily2 == "Zohra" ||
                                            this.state.fontFamily2 == "haldanor"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "haldanor" }}
                                          >
                                            Haldanor
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "alvi_Nastaleeq_Lahori_shipped"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                            "alvi_Nastaleeq_Lahori_shipped"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "alvi_Nastaleeq_Lahori_shipped"
                                            }}
                                          >
                                            Jameel Noori Nastaleeq
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "LearningCurve"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Mishafi" ||
                                            this.state.fontFamily2 ==
                                              "LearningCurve"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LearningCurve"
                                            }}
                                          >
                                            LearningCurve
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "SohoGothicW04_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "Times New Roman" ||
                                            this.state.fontFamily2 ==
                                              "SohoGothicW04_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "SohoGothicW04_Regular"
                                            }}
                                          >
                                            Soho
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "ScheherazadeRegOT"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                            "ScheherazadeRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "ScheherazadeRegOT"
                                            }}
                                          >
                                            ScheherazadeRegOT
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("MintSpirit")
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "TimesNewRomanPSMT" ||
                                            this.state.fontFamily2 ==
                                              "MintSpirit"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "MintSpirit" }}
                                          >
                                            Mint Spirit
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Signika_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "DamascusLight" ||
                                            this.state.fontFamily2 ==
                                              "Signika_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Regular"
                                            }}
                                          >
                                            Signika Regular
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "Signika_Semibold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "DamascusMedium" ||
                                            this.state.fontFamily2 ==
                                              "Signika_Semibold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Semibold"
                                            }}
                                          >
                                            Signika Semibold
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("Tahoma")
                                          }
                                          style={
                                            this.state.fontFamily2 == "Tahoma"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "Tahoma" }}
                                          >
                                            Tahoma
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2("Times")
                                          }
                                          style={
                                            this.state.fontFamily2 == "Farah" ||
                                            this.state.fontFamily2 == "Times"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Times" }}>
                                            Times
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily2(
                                              "TravelingTypewriter"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily2 ==
                                              "DamascusSemiBold" ||
                                            this.state.fontFamily2 ==
                                              "TravelingTypewriter"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "TravelingTypewriter"
                                            }}
                                          >
                                            Traveling Typewriter
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                </View>
                              </View>
                            ) : this.state.focusedInput == 3 ? (
                              <View style={{ width: "100%", marginTop: 10 }}>
                                <View style={{ flexDirection: "row" }}>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("Arial")
                                          }
                                          style={
                                            this.state.fontFamily3 == "Arial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Arial" }}>
                                            Arial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Advertising_Script_Monoline_Trial"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "CourierNewPSMT" ||
                                            this.state.fontFamily3 ==
                                              "Advertising_Script_Monoline_Trial"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "Advertising_Script_Monoline_Trial"
                                            }}
                                          >
                                            Advertising Script Monoline Trial
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Biloxi_Script"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Sharmeen" ||
                                            this.state.fontFamily3 ==
                                              "Biloxi_Script"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Biloxi_Script"
                                            }}
                                          >
                                            Biloxi Script
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "LateefRegOT"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Ulnoreenbld" ||
                                            this.state.fontFamily3 ==
                                              "LateefRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LateefRegOT"
                                            }}
                                          >
                                            LateefRegOT
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Calibri_Bold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Urdu_Emad_Nastaliq" ||
                                            this.state.fontFamily3 ==
                                              "Calibri_Bold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Calibri_Bold"
                                            }}
                                          >
                                            Calibri Bold
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Confetti_Stream"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Urdulife_italic" ||
                                            this.state.fontFamily3 ==
                                              "Confetti_Stream"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Confetti_Stream"
                                            }}
                                          >
                                            Confetti Stream
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "english_essay"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "UrduLink" ||
                                            this.state.fontFamily3 ==
                                              "english_essay"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "english_essay"
                                            }}
                                          >
                                            English essay
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("SinkinSans")
                                          }
                                          style={
                                            this.state.fontFamily3 == "Zafar" ||
                                            this.state.fontFamily3 ==
                                              "SinkinSans"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "SinkinSans" }}
                                          >
                                            Sinkin Sans
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("haldanor")
                                          }
                                          style={
                                            this.state.fontFamily3 == "Zohra" ||
                                            this.state.fontFamily3 == "haldanor"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "haldanor" }}
                                          >
                                            Haldanor
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "alvi_Nastaleeq_Lahori_shipped"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                            "alvi_Nastaleeq_Lahori_shipped"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "alvi_Nastaleeq_Lahori_shipped"
                                            }}
                                          >
                                            Jameel Noori Nastaleeq
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                  <View
                                    style={{ width: "50%", paddingBottom: 40 }}
                                  >
                                    <ScrollView>
                                      <List>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "LearningCurve"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Mishafi" ||
                                            this.state.fontFamily3 ==
                                              "LearningCurve"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "LearningCurve"
                                            }}
                                          >
                                            LearningCurve
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "SohoGothicW04_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "Times New Roman" ||
                                            this.state.fontFamily3 ==
                                              "SohoGothicW04_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily:
                                                "SohoGothicW04_Regular"
                                            }}
                                          >
                                            Soho
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "ScheherazadeRegOT"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                            "ScheherazadeRegOT"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "ScheherazadeRegOT"
                                            }}
                                          >
                                            ScheherazadeRegOT
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("MintSpirit")
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "TimesNewRomanPSMT" ||
                                            this.state.fontFamily3 ==
                                              "MintSpirit"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "MintSpirit" }}
                                          >
                                            Mint Spirit
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Signika_Regular"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "DamascusLight" ||
                                            this.state.fontFamily3 ==
                                              "Signika_Regular"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Regular"
                                            }}
                                          >
                                            Signika Regular
                                          </Text>
                                        </ListItem>

                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "Signika_Semibold"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "DamascusMedium" ||
                                            this.state.fontFamily3 ==
                                              "Signika_Semibold"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "Signika_Semibold"
                                            }}
                                          >
                                            Signika Semibold
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("Tahoma")
                                          }
                                          style={
                                            this.state.fontFamily3 == "Tahoma"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{ fontFamily: "Tahoma" }}
                                          >
                                            Tahoma
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3("Times")
                                          }
                                          style={
                                            this.state.fontFamily3 == "Farah" ||
                                            this.state.fontFamily3 == "Times"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text style={{ fontFamily: "Times" }}>
                                            Times
                                          </Text>
                                        </ListItem>
                                        <ListItem
                                          onPress={() =>
                                            this.changeFontFamily3(
                                              "TravelingTypewriter"
                                            )
                                          }
                                          style={
                                            this.state.fontFamily3 ==
                                              "DamascusSemiBold" ||
                                            this.state.fontFamily3 ==
                                              "TravelingTypewriter"
                                              ? {
                                                  backgroundColor: "#A5B3BC",
                                                  paddingLeft: 5
                                                }
                                              : null
                                          }
                                        >
                                          <Text
                                            style={{
                                              fontFamily: "TravelingTypewriter"
                                            }}
                                          >
                                            Traveling Typewriter
                                          </Text>
                                        </ListItem>
                                      </List>
                                    </ScrollView>
                                  </View>
                                </View>
                              </View>
                            ) : null
                          ) : null}
                          {this.state.tab == 3 ? (
                            this.state.focusedInput == 1 ? (
                              <View
                                style={{
                                  width: "100%",
                                  marginTop: 25,
                                  alignContent: "center",
                                  alignItems: "center"
                                }}
                              >
                                <View style={{ width: "50%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.decreaseFontsize}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: 80,
                                          borderWidth: 0.5,
                                          borderColor: "#ADD8E6",
                                          marginRight: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          -
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.increaseFontsize}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          borderWidth: 0.5,
                                          height: 80,
                                          width: 80,
                                          borderColor: "#ADD8E6",
                                          marginLeft: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          +
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                  <View
                                    style={{
                                      width: "100%",
                                      alignContent: "center",
                                      alignItems: "center",
                                      marginTop: 20
                                    }}
                                  >
                                    <View style={{ width: "70%" }}>
                                      <View style={{ flexDirection: "row" }}>
                                        <View style={{ paddingLeft: 20 }}>
                                          <Text style={{ fontSize: 18 }}>
                                            Outline
                                          </Text>
                                        </View>
                                        <CheckBox
                                          onPress={() => this.text1Outline()}
                                          checked={this.state.text1Outline}
                                        />
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            ) : this.state.focusedInput == 2 ? (
                              <View
                                style={{
                                  width: "100%",
                                  marginTop: 25,
                                  alignContent: "center",
                                  alignItems: "center"
                                }}
                              >
                                <View style={{ width: "50%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.decreaseFontsize2}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: 80,
                                          borderWidth: 0.5,
                                          borderColor: "#ADD8E6",
                                          marginRight: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          -
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.increaseFontsize2}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          borderWidth: 0.5,
                                          height: 80,
                                          width: 80,
                                          borderColor: "#ADD8E6",
                                          marginLeft: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          +
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                  <View
                                    style={{
                                      width: "100%",
                                      alignContent: "center",
                                      alignItems: "center",
                                      marginTop: 20
                                    }}
                                  >
                                    <View style={{ width: "70%" }}>
                                      <View style={{ flexDirection: "row" }}>
                                        <View style={{ paddingLeft: 20 }}>
                                          <Text style={{ fontSize: 18 }}>
                                            Outline
                                          </Text>
                                        </View>
                                        <CheckBox
                                          onPress={() => this.text2Outline()}
                                          checked={this.state.text2Outline}
                                        />
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            ) : this.state.focusedInput == 3 ? (
                              <View
                                style={{
                                  width: "100%",
                                  marginTop: 25,
                                  alignContent: "center",
                                  alignItems: "center"
                                }}
                              >
                                <View style={{ width: "50%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.decreaseFontsize3}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: 80,
                                          borderWidth: 0.5,
                                          borderColor: "#ADD8E6",
                                          marginRight: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          -
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: 80 }}
                                      onPress={this.increaseFontsize3}
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          borderWidth: 0.5,
                                          height: 80,
                                          width: 80,
                                          borderColor: "#ADD8E6",
                                          marginLeft: 20,
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center"
                                        }}
                                      >
                                        <Text
                                          style={{
                                            fontSize: 44,
                                            fontWeight: "bold"
                                          }}
                                        >
                                          +
                                        </Text>
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                  <View
                                    style={{
                                      width: "100%",
                                      alignContent: "center",
                                      alignItems: "center",
                                      marginTop: 20
                                    }}
                                  >
                                    <View style={{ width: "70%" }}>
                                      <View style={{ flexDirection: "row" }}>
                                        <View style={{ paddingLeft: 20 }}>
                                          <Text style={{ fontSize: 18 }}>
                                            Outline
                                          </Text>
                                        </View>
                                        <CheckBox
                                          onPress={() => this.text3Outline()}
                                          checked={this.state.text3Outline}
                                        />
                                      </View>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            ) : null
                          ) : null}
                          {this.state.tab == 4 ? (
                            this.state.focusedInput == 1 ? (
                              <View
                                style={{
                                  width: "100%",
                                  height: this.state.keyboardHeight,
                                  paddingBottom: 60
                                }}
                              >
                                <ScrollView style={{ paddingBottom: 50 }}>
                                  <View
                                    style={{
                                      flexDirection: "row",
                                      flexWrap: "wrap"
                                    }}
                                  >
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff3300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff3300"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff3300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffffff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffffff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffffff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#000000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#000000"
                                        }}
                                      >
                                        {this.state.fontColor == "#000000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#009933")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009933"
                                        }}
                                      >
                                        {this.state.fontColor == "#009933" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#6600ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#6600ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#6600ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff9900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff9900"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff9900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#663300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#663300"
                                        }}
                                      >
                                        {this.state.fontColor == "#663300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffff80")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffff80"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffff80" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff99ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff99ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff99ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#b3ccff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#b3ccff"
                                        }}
                                      >
                                        {this.state.fontColor == "#b3ccff" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ccffcc")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ccffcc"
                                        }}
                                      >
                                        {this.state.fontColor == "#ccffcc" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff0000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff0000"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff0000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#808080")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#808080"
                                        }}
                                      >
                                        {this.state.fontColor == "#808080" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#e6e6e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6e6e6"
                                        }}
                                      >
                                        {this.state.fontColor == "#e6e6e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#f2f2f2")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#f2f2f2"
                                        }}
                                      >
                                        {this.state.fontColor == "#f2f2f2" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#e6eeff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6eeff"
                                        }}
                                      >
                                        {this.state.fontColor == "#e6eeff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#993300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#993300"
                                        }}
                                      >
                                        {this.state.fontColor == "#993300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#990099")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#990099"
                                        }}
                                      >
                                        {this.state.fontColor == "#990099" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#0066ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#0066ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#0066ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#e6f0ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6f0ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#e6f0ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#003d99")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#003d99"
                                        }}
                                      >
                                        {this.state.fontColor == "#003d99" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#66a3ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#66a3ff"
                                        }}
                                      >
                                        {this.state.fontColor == "#66a3ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#00ff00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#00ff00"
                                        }}
                                      >
                                        {this.state.fontColor == "#00ff00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#009900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009900"
                                        }}
                                      >
                                        {this.state.fontColor == "#009900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#006600")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#006600"
                                        }}
                                      >
                                        {this.state.fontColor == "#006600" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#4d4d4d")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#4d4d4d"
                                        }}
                                      >
                                        {this.state.fontColor == "#4d4d4d" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#fff5e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#fff5e6"
                                        }}
                                      >
                                        {this.state.fontColor == "#fff5e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#ffc266")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffc266"
                                        }}
                                      >
                                        {this.state.fontColor == "#ffc266" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor("#995c00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#995c00"
                                        }}
                                      >
                                        {this.state.fontColor == "#995c00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </ScrollView>
                              </View>
                            ) : this.state.focusedInput == 2 ? (
                              <View
                                style={{
                                  width: "100%",
                                  height: this.state.keyboardHeight,
                                  paddingBottom: 60
                                }}
                              >
                                <ScrollView style={{ paddingBottom: 50 }}>
                                  <View
                                    style={{
                                      flexDirection: "row",
                                      flexWrap: "wrap"
                                    }}
                                  >
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff3300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff3300"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff3300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffffff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffffff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffffff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#000000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#000000"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#000000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#009933")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009933"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#009933" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#6600ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#6600ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#6600ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff9900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff9900"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff9900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#663300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#663300"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#663300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffff80")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffff80"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffff80" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff99ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff99ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff99ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#b3ccff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#b3ccff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#b3ccff" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ccffcc")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ccffcc"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ccffcc" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff0000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff0000"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff0000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#808080")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#808080"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#808080" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#e6e6e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6e6e6"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#e6e6e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#f2f2f2")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#f2f2f2"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#f2f2f2" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#e6eeff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6eeff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#e6eeff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#993300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#993300"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#993300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#990099")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#990099"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#990099" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#0066ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#0066ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#0066ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#e6f0ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6f0ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#e6f0ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#003d99")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#003d99"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#003d99" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#66a3ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#66a3ff"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#66a3ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#00ff00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#00ff00"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#00ff00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#009900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009900"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#009900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#006600")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#006600"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#006600" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#4d4d4d")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#4d4d4d"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#4d4d4d" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#fff5e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#fff5e6"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#fff5e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#ffc266")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffc266"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#ffc266" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor2("#995c00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#995c00"
                                        }}
                                      >
                                        {this.state.fontColor2 == "#995c00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </ScrollView>
                              </View>
                            ) : this.state.focusedInput == 3 ? (
                              <View
                                style={{
                                  width: "100%",
                                  height: this.state.keyboardHeight,
                                  paddingBottom: 60
                                }}
                              >
                                <ScrollView style={{ paddingBottom: 50 }}>
                                  <View
                                    style={{
                                      flexDirection: "row",
                                      flexWrap: "wrap"
                                    }}
                                  >
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff3300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff3300"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff3300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffffff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffffff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffffff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#000000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#000000"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#000000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#009933")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009933"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#009933" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#6600ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#6600ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#6600ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff9900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff9900"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff9900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#663300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#663300"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#663300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffff80")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffff80"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffff80" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff99ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff99ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff99ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#b3ccff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#b3ccff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#b3ccff" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ccffcc")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ccffcc"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ccffcc" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff0000")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff0000"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff0000" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#808080")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#808080"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#808080" ? (
                                          <Icon
                                            style={{ color: "#000" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#e6e6e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6e6e6"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#e6e6e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#f2f2f2")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#f2f2f2"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#f2f2f2" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#e6eeff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6eeff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#e6eeff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#993300")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#993300"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#993300" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffe6ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffe6ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffe6ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#990099")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#990099"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#990099" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ff00ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ff00ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ff00ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#0066ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#0066ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#0066ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#e6f0ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6f0ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#e6f0ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#003d99")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#003d99"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#003d99" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#66a3ff")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#66a3ff"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#66a3ff" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#e6ffe6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#e6ffe6"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#e6ffe6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#00ff00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#00ff00"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#00ff00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#009900")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#009900"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#009900" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#006600")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#006600"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#006600" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#4d4d4d")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#4d4d4d"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#4d4d4d" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#fff5e6")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#fff5e6"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#fff5e6" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#ffc266")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#ffc266"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#ffc266" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity
                                      style={{ height: 80, width: "25%" }}
                                      onPress={() =>
                                        this.changeFontColor3("#995c00")
                                      }
                                    >
                                      <View
                                        style={{
                                          paddingHorizontal: 10,
                                          height: 80,
                                          width: "100%",
                                          alignItems: "center",
                                          alignContent: "center",
                                          justifyContent: "center",
                                          backgroundColor: "#995c00"
                                        }}
                                      >
                                        {this.state.fontColor3 == "#995c00" ? (
                                          <Icon
                                            style={{ color: "#808080" }}
                                            name="checkmark-circle"
                                          />
                                        ) : null}
                                      </View>
                                    </TouchableOpacity>
                                  </View>
                                </ScrollView>
                              </View>
                            ) : null
                          ) : null}
                        </View>
                      ) : null}
              {this.state.showBar ? (
                <View
                  style={{
                    position: "absolute",
                    bottom:
                      this.state.keyboardHeight != 0 || this.state.showtab
                        ? this.state.keyboardSize
                        : null,
                    top:
                      this.state.keyboardHeight == 0 && !this.state.showtab
                        ? deviceHeight - 40
                        : null,
                    right: 0,
                    zIndex: 10
                  }}
                >
                  <View
                    style={{
                      width: deviceWidth,
                      height: 40,
                      backgroundColor: "#EAEAEA",
                      justifyContent: "center"
                    }}
                  >
                    <View style={{ flexDirection: "row" }}>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center",
                          borderBottomWidth: 2,
                          borderBottomColor:
                            this.state.tab == 1 ? "#3C8BDB" : "transparent"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            Keyboard.dismiss();
                            this.setState({
                              tab: 1,
                              showtab: true,
                              // focusedInput: this.state.text3
                              //   ? 3
                              //   : this.state.text2
                              //   ? 2
                              //   : this.state.text1
                              //   ? 1
                              //   : null,
                              keyboardHeight: this.state.keyboardSize
                            });
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/bgcolor-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center",
                          borderBottomWidth: 2,
                          borderBottomColor:
                            this.state.tab == 2 ? "#3C8BDB" : "transparent"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              tab: 2,
                              showtab: true,
                              // focusedInput: this.state.text3
                              //   ? 3
                              //   : this.state.text2
                              //   ? 2
                              //   : this.state.text1
                              //   ? 1
                              //   : null,
                              keyboardHeight: this.state.keyboardSize
                            });
                            Keyboard.dismiss();
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/font-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center",
                          borderBottomWidth: 2,
                          borderBottomColor:
                            this.state.tab == 3 ? "#3C8BDB" : "transparent"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              tab: 3,
                              showtab: true,
                              // focusedInput: this.state.text3
                              //   ? 3
                              //   : this.state.text2
                              //   ? 2
                              //   : this.state.text1
                              //   ? 1
                              //   : null,
                              keyboardHeight: this.state.keyboardSize
                            });
                            Keyboard.dismiss();
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/size-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center",
                          borderBottomWidth: 2,
                          borderBottomColor:
                            this.state.tab == 4 ? "#3C8BDB" : "transparent"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              tab: 4,
                              showtab: true,
                              // focusedInput: this.state.text3
                              //   ? 3
                              //   : this.state.text2
                              //   ? 2
                              //   : this.state.text1
                              //   ? 1
                              //   : null,
                              keyboardHeight: this.state.keyboardSize
                            });
                            Keyboard.dismiss();
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/color-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.addText();
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/plus.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center",
                          borderBottomWidth: 2,
                          borderBottomColor:
                            this.state.tab == 6 ? "#3C8BDB" : "transparent"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({ tab: 6 });
                            if (this.state.focusedInput == 1) {
                              this.textInputField.focus();
                            }
                            if (this.state.focusedInput == 2) {
                              this.text2InputField.focus();
                            }
                            if (this.state.focusedInput == 3) {
                              this.text3InputField.focus();
                            }
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/keyboard-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              showtab: false,
                              tab: 0,
                              keyboardHeight: 0
                            });
                            Keyboard.dismiss();
                            this.ActionSheet.show();
                          }}
                        >
                          <Image
                            style={{
                              width: 35,
                              height: 35,
                              resizeMode: "contain"
                            }}
                            source={require("../../assets/images/done-icon.png")}
                          />
                        </TouchableOpacity>
                      </View>
                      <View
                        style={{
                          width: "12.5%",
                          alignContent: "center",
                          alignItems: "center"
                        }}
                      >
                        {this.state.keyboardHeight == 0 ? (
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                showtab: false,
                                tab: 0,
                                keyboardHeight: 0
                              });
                              Keyboard.dismiss();
                              this.showActionSheet();
                            }}
                          >
                            <Icon name="ios-more" />
                          </TouchableOpacity>
                        ) : (
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                showtab: false,
                                tab: 0,
                                keyboardHeight: 0
                              });
                              Keyboard.dismiss();
                            }}
                          >
                            <Image
                              style={{
                                width: 35,
                                height: 35,
                                resizeMode: "contain"
                              }}
                              source={require("../../assets/images/hide-tabs-arrow-down.png")}
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    </View>
                  </View>
                </View>
              ) : null}
            </View>
          </Content>
        ) : (
          <ViewShot
            style={{ width: "100%", height: this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? 250 : deviceHeight - 80 : deviceHeight, padding:0, margin:0  }}
            ref="viewShot"
            options={{ format: "png", quality: 0.9 }}
          >
             
            <ImageBackground
              source={{ uri: ImageUri }}
              style={{
                width:  deviceWidth,
                height:  this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? 250 : deviceHeight - 80 : deviceHeight
              }}
              imageStyle={
                !ImageUri
                  ? { resizeMode: "stretch" }
                  : {
                      resizeMode: this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? null : null : this.state.bgImageSize
                    }
              }
            >
             <View
                style={{
                  width: "100%",
                  height: deviceHeight - 40,
                  backgroundColor: this.state.backgroundColor
                }}
              >
                <ViewShot
                  style={{ width: "100%", height: deviceHeight }}
                  ref="viewShot_share"
                  options={{ format: "png", quality: 0.9 }}
                >
                  {this.state.text1 ? (
                    <Draggable
                      reverse={false}
                      x={10}
                      y={10}
                      pressInDrag={() =>
                        Platform.OS == "android" ? this.focusText1() : null
                      }
                      pressDragRelease={() =>
                        Platform.OS == "android"
                          ? this.textInputField.focus()
                          : null
                      }
                    >
                      <Form
                        style={{
                          width: deviceWidth - 20,
                          paddingTop: 30,
                          zIndex: -1
                        }}
                      >
                        {!this.state.hidden ? (
                          Platform.OS == "android" ? (
                            <View
                              style={{
                                backgroundColor: "#3C8BDB",
                                width: 20,
                                height: 20,
                                borderRadius: 20,
                                alignItems: "center",
                                alignContent: "center",
                                justifyContent: "center"
                              }}
                            >
                              <Icon style={{ fontSize: 15 }} name="create" />
                            </View>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.showTextActionSheet()}
                              style={{marginLeft :toAlign == "right" ? "auto" : null}}
                            >
                              <View>
                                <Image
                                  style={{
                                    width: 30,
                                    height: 30,
                                    resizeMode: "contain"
                                  }}
                                  source={require("../../assets/images/edit-pen-icon.png")}
                                />
                              </View>
                            </TouchableOpacity>
                          )
                        ) : null}

                        <TextInput
                          value={this.state.message}
                          placeholder={!this.state.hidden ? "Enter text here" : ""}
                          ref={ref => {
                            this.textInputField = ref;
                          }}
                          style={{
                            width: "100%",
                            fontSize: this.state.fontSize,
                            color: this.state.fontColor,
                            fontFamily: this.state.fontFamily,
                            height: "auto",
                            textAlign: toAlign,
                            textShadowRadius: this.state.text1Outline ? 10 : 0,
                            zIndex: -1,
                            textShadowOffset: { width: 1, height: 1 },
                            textShadowColor: "#686F74"
                          }}
                          onChangeText={message => {
                            this.setState({ message: message });
                            if (message == "") {
                              this.setState({ fontFamily: "Arial" });
                            }
                          }}
                          onFocus={() =>
                            this.setState({
                              focusedInput: 1,
                              showtab: true,
                              tab: 6
                            })
                          }
                          multiline={true}
                          maxLength={953}
                          enablesReturnKeyAutomatically={true}
                        />
                      </Form>
                    </Draggable>
) : null}
                  {this.state.text2 ? (
                    <Draggable
                      reverse={false}
                      x={10}
                      y={100}
                      pressInDrag={() => this.showText2ActionSheet()}
                      pressDragRelease={() =>
                        Platform.OS == "android"
                          ? this.text2InputField.focus()
                          : null
                      }
                    >
                      <Form
                        style={{
                          width: deviceWidth - 20,
                          paddingVertical: 30,
                          zIndex: -1
                        }}
                      >
                        {!this.state.hidden ? (
                          Platform.OS == "android" ? (
                            <View
                              style={{
                                backgroundColor: "#3C8BDB",
                                width: 20,
                                height: 20,
                                borderRadius: 20,
                                alignItems: "center",
                                alignContent: "center",
                                justifyContent: "center"
                              }}
                            >
                              <Icon style={{ fontSize: 15 }} name="create" />
                            </View>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.showText2ActionSheet()}
                              style={{marginLeft :toAlign2 == "right" ? "auto" : null}}
                            >
                              <View>
                                <Image
                                  style={{
                                    width: 30,
                                    height: 30,
                                    resizeMode: "contain"
                                  }}
                                  source={require("../../assets/images/edit-pen-icon.png")}
                                />
                              </View>
                            </TouchableOpacity>
                          )
                        ) : null}
                        <TextInput
                          placeholder={!this.state.hidden ? "Enter text here" : ""}
                          value={this.state.message2}
                          ref={ref => {
                            this.text2InputField = ref;
                          }}
                          style={{
                            width: "100%",
                            fontSize: this.state.fontSize2,
                            color: this.state.fontColor2,
                            fontFamily: this.state.fontFamily2,
                            height: "auto",
                            textAlign: toAlign2,
                            textShadowRadius: this.state.text2Outline ? 10 : 0,
                            zIndex: -1,
                            textShadowOffset: { width: 1, height: 1 },
                            textShadowColor: "#686F74"
                          }}
                          onChangeText={message2 => {
                            this.setState({ message2: message2 });
                            if (message2 == "") {
                              this.setState({ fontFamily2: "Arial" });
                            }
                          }}
                          onFocus={() =>
                            this.setState({
                              focusedInput: 2,
                              showtab: true,
                              tab: 6
                            })
                          }
                          multiline={true}
                          maxLength={953}
                          enablesReturnKeyAutomatically={true}
                        />
                      </Form>
                    </Draggable>
 ) : null}
                  {this.state.text3 ? (
                    <Draggable
                      reverse={false}
                      x={10}
                      y={200}
                      pressInDrag={() => this.showText3ActionSheet()}
                      pressDragRelease={() =>
                        Platform.OS == "android"
                          ? this.text3InputField.focus()
                          : null
                      }
                    >
                      <Form
                        style={{
                          width: deviceWidth - 20,
                          paddingVertical: 30,
                          zIndex: -1
                        }}
                      >
                        {!this.state.hidden ? (
                          Platform.OS == "android" ? (
                            <View
                              style={{
                                backgroundColor: "#3C8BDB",
                                width: 20,
                                height: 20,
                                borderRadius: 20,
                                alignItems: "center",
                                alignContent: "center",
                                justifyContent: "center"
                              }}
                            >
                              <Icon style={{ fontSize: 15 }} name="create" />
                            </View>
                          ) : (
                            <TouchableOpacity
                              onPress={() => this.showTextActionSheet()}
                              style={{marginLeft :toAlign3 == "right" ? "auto" : null}}
                            >
                              <View>
                                <Image
                                  style={{
                                    width: 30,
                                    height: 30,
                                    resizeMode: "contain"
                                  }}
                                  source={require("../../assets/images/edit-pen-icon.png")}
                                />
                              </View>
                            </TouchableOpacity>
                          )
                        ) : null}
                        <TextInput
                          ref={ref => {
                            this.text3InputField = ref;
                          }}
                          placeholder={!this.state.hidden ? "Enter text here" : ""}
                          value={this.state.message3}
                          ref={ref => {
                            this.text3InputField = ref;
                          }}
                          style={{
                            width: "100%",
                            fontSize: this.state.fontSize3,
                            color: this.state.fontColor3,
                            fontFamily: this.state.fontFamily3,
                            height: "auto",
                            textAlign: toAlign3,
                            textShadowRadius: this.state.text3Outline ? 10 : 0,
                            zIndex: -1,
                            textShadowOffset: { width: 1, height: 1 },
                            textShadowColor: "#686F74"
                          }}
                          onChangeText={message3 => {
                            this.setState({ message3: message3 });
                            if (message3 == "") {
                              this.setState({ fontFamily3: "Arial" });
                            }
                          }}
                          onFocus={() =>
                            this.setState({
                              focusedInput: 3,
                              showtab: true,
                              tab: 6
                            })
                          }
                          multiline={true}
                          maxLength={953}
                          enablesReturnKeyAutomatically={true}
                        />
                      </Form>
                    </Draggable>

                  ) : null}
                  <View
                    style={ this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ? {
                      position: "absolute",
                      top: 160,
                      right: 0,
                      zIndex: 10
                    } : {
                      position: "absolute",
                      bottom: 100,
                      right: 0,
                      zIndex: 10
                    } : {
                      position: "absolute",
                      bottom: 100,
                      right: 0,
                      zIndex: 10
                    } 
                      }
                  >
                    <Image
                      style={ this.state.bgImageSize == "contain" ? this.state.containWidth > this.state.containHeight ?
                        { height: 75, width: 75, resizeMode: "contain" }
                        :
                        { height: 100, width: 100, resizeMode: "contain" }:
                        { height: 100, width: 100, resizeMode: "contain" }}
                      source={require("../../assets/images/watermark.png")}
                    />
                  </View>
                  {this.state.showDone_pannel ? (
                    <View
                      style={{
                        position: "absolute",
                        bottom: 80,
                        left: 40,
                        zIndex: 9999,
                        backgroundColor: "#eaeaea",
                        width: deviceWidth - 80,
                        borderRadius: 20
                      }}
                    >
                      <TouchableOpacity
                        onPress={() => this.captureImage()}
                        style={{
                          width: "100%",
                          alignItems: "center",
                          alignContent: "center"
                        }}
                      >
                        <View style={{ paddingVertical: 10 }}>
                          <Text style={{ fontSize: 20 }}>Save To Photos</Text>
                        </View>

                        <View
                          style={{
                            width: "95%",
                            borderBottomWidth: 0.5,
                            borderBottomColor: "#808080",
                            marginTop: 5
                          }}
                        ></View>
                      </TouchableOpacity>

                      <TouchableOpacity
                        onPress={() => this.onShare()}
                        style={{
                          width: "100%",
                          alignItems: "center",
                          alignContent: "center"
                        }}
                      >
                        <View style={{ paddingVertical: 10 }}>
                          <Text style={{ fontSize: 20 }}>Share</Text>
                        </View>

                        <View
                          style={{
                            width: "95%",
                            borderBottomWidth: 0.5,
                            borderBottomColor: "#808080",
                            marginTop: 5
                          }}
                        ></View>
                      </TouchableOpacity>
                      <TouchableOpacity
                        onPress={() =>
                          this.setState({ showDone_pannel: false })
                        }
                        style={{
                          width: "100%",
                          alignItems: "center",
                          alignContent: "center"
                        }}
                      >
                        <View style={{ paddingVertical: 10 }}>
                          <Text
                            style={{
                              fontSize: 20,
                              color: "red",
                              fontWeight: "600"
                            }}
                          >
                            Cancel
                          </Text>
                        </View>

                        <View
                          style={{
                            width: "95%",
                            borderBottomWidth: 0.5,
                            borderBottomColor: "#808080",
                            marginTop: 5
                          }}
                        ></View>
                      </TouchableOpacity>
                    </View>
                  ) : null}
                  {this.state.showBar ? (
                    <View
                      style={{
                        position: "absolute",
                        bottom:
                          this.state.keyboardHeight != 0 || this.state.showtab
                            ? this.state.keyboardSize
                            : null,
                        top:
                          this.state.keyboardHeight == 0 && !this.state.showtab
                            ? deviceHeight - 40
                            : null,
                        right: 0,
                        zIndex: 9999
                      }}
                    >
                      <View
                        style={{
                          width: deviceWidth,
                          height: 40,
                          backgroundColor: "#EAEAEA",
                          justifyContent: "center"
                        }}
                      >
                        <View style={{ flexDirection: "row" }}>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center",
                              borderBottomWidth: 2,
                              borderBottomColor:
                                this.state.tab == 1 ? "#3C8BDB" : "transparent"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                Keyboard.dismiss();
                                this.setState({
                                  tab: 1,
                                  showtab: true,
                                  // focusedInput: this.state.text3
                                  //   ? 3
                                  //   : this.state.text2
                                  //   ? 2
                                  //   : this.state.text1
                                  //   ? 1
                                  //   : null,
                                  keyboardHeight: this.state.keyboardSize
                                });
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/bgcolor-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center",
                              borderBottomWidth: 2,
                              borderBottomColor:
                                this.state.tab == 2 ? "#3C8BDB" : "transparent"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({
                                  tab: 2,
                                  showtab: true,
                                  // focusedInput: this.state.text3
                                  //   ? 3
                                  //   : this.state.text2
                                  //   ? 2
                                  //   : this.state.text1
                                  //   ? 1
                                  //   : null,
                                  keyboardHeight: this.state.keyboardSize
                                });

                                Keyboard.dismiss();
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/font-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center",
                              borderBottomWidth: 2,
                              borderBottomColor:
                                this.state.tab == 3 ? "#3C8BDB" : "transparent"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({
                                  tab: 3,
                                  showtab: true,
                                  // focusedInput: this.state.text3
                                  //   ? 3
                                  //   : this.state.text2
                                  //   ? 2
                                  //   : this.state.text1
                                  //   ? 1
                                  //   : null,
                                  keyboardHeight: this.state.keyboardSize
                                });

                                Keyboard.dismiss();
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/size-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center",
                              borderBottomWidth: 2,
                              borderBottomColor:
                                this.state.tab == 4 ? "#3C8BDB" : "transparent"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({
                                  tab: 4,
                                  showtab: true,
                                  // focusedInput: this.state.text3
                                  //   ? 3
                                  //   : this.state.text2
                                  //   ? 2
                                  //   : this.state.text1
                                  //   ? 1
                                  //   : null,
                                  keyboardHeight: this.state.keyboardSize
                                });

                                Keyboard.dismiss();
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/color-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.addText();
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/plus.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center",
                              borderBottomWidth: 2,
                              borderBottomColor:
                                this.state.tab == 6 ? "#3C8BDB" : "transparent"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({ tab: 6 });
                                if (this.state.focusedInput == 1) {
                                  this.textInputField.focus();
                                }
                                if (this.state.focusedInput == 2) {
                                  this.text2InputField.focus();
                                }
                                if (this.state.focusedInput == 3) {
                                  this.text3InputField.focus();
                                }
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/keyboard-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <TouchableOpacity
                              onPress={() => {
                                this.setState({
                                  tab: 0,
                                  showtab: false,
                                  showDone_pannel: true,
                                  keyboardHeight: 0
                                });
                                Keyboard.dismiss();
                              }}
                            >
                              <Image
                                style={{
                                  width: 35,
                                  height: 35,
                                  resizeMode: "contain"
                                }}
                                source={require("../../assets/images/done-icon.png")}
                              />
                            </TouchableOpacity>
                          </View>
                          <View
                            style={{
                              width: "12.5%",
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            {this.state.keyboardHeight == 0 ? (
                              <TouchableOpacity
                                onPress={() => {
                                  this.setState({
                                    showtab: false,
                                    tab: 0,
                                    keyboardHeight: 0
                                  });
                                  Keyboard.dismiss();
                                  this.showActionSheet();
                                }}
                              >
                                <Icon name="ios-more" />
                              </TouchableOpacity>
                            ) : (
                              <TouchableOpacity
                                onPress={() => {
                                  this.setState({
                                    showtab: false,
                                    tab: 0,
                                    keyboardHeight: 0
                                  });
                                  Keyboard.dismiss();
                                }}
                              >
                                <Image
                                  style={{
                                    width: 35,
                                    height: 35,
                                    resizeMode: "contain"
                                  }}
                                  source={require("../../assets/images/hide-tabs-arrow-down.png")}
                                />
                              </TouchableOpacity>
                            )}
                          </View>
                        </View>
                      </View>
                    </View>
                  ) : null}

                  {this.state.showtab ? (
                    <View
                      style={{
                        width: deviceWidth,
                        height: this.state.keyboardHeight,
                        backgroundColor: "#fff",
                        position: "absolute",
                        bottom: 0,
                        zIndex: 9999
                      }}
                    >
                      {this.state.tab == 1 ? (
                        <ScrollView style={{ paddingBottom: 100 }}>
                          <View
                            style={{
                              width: "100%",
                              alignItems: "center",
                              alignContent: "center",
                              paddingBottom: 40
                            }}
                          >
                            <View style={{ width: "50%", marginVertical: 20 }}>
                              <Button
                                primary
                                style={{
                                  alignItems: "center",
                                  alignContent: "center",
                                  justifyContent: "center"
                                }}
                                onPress={this._pickImage}
                              >
                                <Text
                                  style={{
                                    color: "#fff",
                                    fontSize: 16,
                                    fontWeight: "bold"
                                  }}
                                >
                                  Browse
                                </Text>
                              </Button>
                            </View>
                            <View
                              style={{ flexDirection: "row", flexWrap: "wrap" }}
                            >
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff3300")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff3300"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff3300" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffffff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffffff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffffff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#000000")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#000000"
                                  }}
                                >
                                  {this.state.backgroundColor == "#000000" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#009933")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#009933"
                                  }}
                                >
                                  {this.state.backgroundColor == "#009933" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#6600ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#6600ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#6600ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff9900")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff9900"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff9900" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff00ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff00ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff00ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#663300")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#663300"
                                  }}
                                >
                                  {this.state.backgroundColor == "#663300" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffff80")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffff80"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffff80" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff99ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff99ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff99ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#b3ccff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#b3ccff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#b3ccff" ? (
                                    <Icon
                                      style={{ color: "#000" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ccffcc")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ccffcc"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ccffcc" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff0000")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff0000"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff0000" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#808080")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#808080"
                                  }}
                                >
                                  {this.state.backgroundColor == "#808080" ? (
                                    <Icon
                                      style={{ color: "#000" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#e6e6e6")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#e6e6e6"
                                  }}
                                >
                                  {this.state.backgroundColor == "#e6e6e6" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#f2f2f2")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#f2f2f2"
                                  }}
                                >
                                  {this.state.backgroundColor == "#f2f2f2" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffe6ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffe6ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffe6ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#e6ffe6")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#e6ffe6"
                                  }}
                                >
                                  {this.state.backgroundColor == "#e6ffe6" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffe6ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffe6ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffe6ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#e6eeff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#e6eeff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#e6eeff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#993300")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#993300"
                                  }}
                                >
                                  {this.state.backgroundColor == "#993300" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffe6ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffe6ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffe6ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#990099")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#990099"
                                  }}
                                >
                                  {this.state.backgroundColor == "#990099" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ff00ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ff00ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ff00ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#0066ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#0066ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#0066ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#e6f0ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#e6f0ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#e6f0ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#003d99")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#003d99"
                                  }}
                                >
                                  {this.state.backgroundColor == "#003d99" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#66a3ff")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#66a3ff"
                                  }}
                                >
                                  {this.state.backgroundColor == "#66a3ff" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#e6ffe6")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#e6ffe6"
                                  }}
                                >
                                  {this.state.backgroundColor == "#e6ffe6" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#00ff00")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#00ff00"
                                  }}
                                >
                                  {this.state.backgroundColor == "#00ff00" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#009900")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#009900"
                                  }}
                                >
                                  {this.state.backgroundColor == "#009900" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#006600")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#006600"
                                  }}
                                >
                                  {this.state.backgroundColor == "#006600" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#4d4d4d")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#4d4d4d"
                                  }}
                                >
                                  {this.state.backgroundColor == "#4d4d4d" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#fff5e6")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#fff5e6"
                                  }}
                                >
                                  {this.state.backgroundColor == "#fff5e6" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#ffc266")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#ffc266"
                                  }}
                                >
                                  {this.state.backgroundColor == "#ffc266" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                              <TouchableOpacity
                                style={{ height: 80, width: "25%" }}
                                onPress={() =>
                                  this.changeBackgroundColor("#995c00")
                                }
                              >
                                <View
                                  style={{
                                    paddingHorizontal: 10,
                                    height: 80,
                                    width: "100%",
                                    alignItems: "center",
                                    alignContent: "center",
                                    justifyContent: "center",
                                    backgroundColor: "#995c00"
                                  }}
                                >
                                  {this.state.backgroundColor == "#995c00" ? (
                                    <Icon
                                      style={{ color: "#808080" }}
                                      name="checkmark-circle"
                                    />
                                  ) : null}
                                </View>
                              </TouchableOpacity>
                            </View>
                          </View>
                        </ScrollView>
                      ) : null}
                      {this.state.tab == 2 ? (
                        this.state.focusedInput == 1 ? (
                          <View style={{ width: "100%", marginTop: 10 }}>
                            <View style={{ flexDirection: "row" }}>
                              <View style={{ width: "50%", paddingBottom: 0 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Arial")
                                      }
                                      style={
                                        this.state.fontFamily == "Arial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Arial" }}>
                                        Arial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "Advertising_Script_Monoline_Trial"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "CourierNewPSMT" ||
                                        this.state.fontFamily ==
                                          "Advertising_Script_Monoline_Trial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "Advertising_Script_Monoline_Trial"
                                        }}
                                      >
                                        Advertising Script Monoline Trial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Biloxi_Script")
                                      }
                                      style={
                                        this.state.fontFamily == "Sharmeen" ||
                                        this.state.fontFamily == "Biloxi_Script"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Biloxi_Script" }}
                                      >
                                        Biloxi Script
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("LateefRegOT")
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "Ulnoreenbld" ||
                                        this.state.fontFamily == "LateefRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LateefRegOT" }}
                                      >
                                        LateefRegOT
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Calibri_Bold")
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "Urdu_Emad_Nastaliq" ||
                                        this.state.fontFamily == "Calibri_Bold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Calibri_Bold" }}
                                      >
                                        Calibri Bold
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Confetti_Stream")
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "Urdulife_italic" ||
                                        this.state.fontFamily ==
                                          "Confetti_Stream"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Confetti_Stream"
                                        }}
                                      >
                                        Confetti Stream
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("english_essay")
                                      }
                                      style={
                                        this.state.fontFamily == "UrduLink" ||
                                        this.state.fontFamily == "english_essay"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "english_essay" }}
                                      >
                                        English essay
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("SinkinSans")
                                      }
                                      style={
                                        this.state.fontFamily == "Zafar" ||
                                        this.state.fontFamily == "SinkinSans"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "SinkinSans" }}
                                      >
                                        Sinkin Sans
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("haldanor")
                                      }
                                      style={
                                        this.state.fontFamily == "Zohra" ||
                                        this.state.fontFamily == "haldanor"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "haldanor" }}>
                                        Haldanor
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "alvi_Nastaleeq_Lahori_shipped"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                        "alvi_Nastaleeq_Lahori_shipped"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "alvi_Nastaleeq_Lahori_shipped"
                                        }}
                                      >
                                        Jameel Noori Nastaleeq
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                              <View style={{ width: "50%", paddingBottom: 0 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("LearningCurve")
                                      }
                                      style={
                                        this.state.fontFamily == "Mishafi" ||
                                        this.state.fontFamily == "LearningCurve"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LearningCurve" }}
                                      >
                                        LearningCurve
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "SohoGothicW04_Regular"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "Times New Roman" ||
                                        this.state.fontFamily ==
                                          "SohoGothicW04_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "SohoGothicW04_Regular"
                                        }}
                                      >
                                        Soho
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "ScheherazadeRegOT"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                        "ScheherazadeRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "ScheherazadeRegOT"
                                        }}
                                      >
                                        ScheherazadeRegOT
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("MintSpirit")
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "TimesNewRomanPSMT" ||
                                        this.state.fontFamily == "MintSpirit"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "MintSpirit" }}
                                      >
                                        Mint Spirit
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Signika_Regular")
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "DamascusLight" ||
                                        this.state.fontFamily ==
                                          "Signika_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Regular"
                                        }}
                                      >
                                        Signika Regular
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "Signika_Semibold"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "DamascusMedium" ||
                                        this.state.fontFamily ==
                                          "Signika_Semibold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Semibold"
                                        }}
                                      >
                                        Signika Semibold
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Tahoma")
                                      }
                                      style={
                                        this.state.fontFamily == "Tahoma"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Tahoma" }}>
                                        Tahoma
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily("Times")
                                      }
                                      style={
                                        this.state.fontFamily == "Farah" ||
                                        this.state.fontFamily == "Times"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Times" }}>
                                        Times
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily(
                                          "TravelingTypewriter"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily ==
                                          "DamascusSemiBold" ||
                                        this.state.fontFamily ==
                                          "TravelingTypewriter"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "TravelingTypewriter"
                                        }}
                                      >
                                        Traveling Typewriter
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                            </View>
                          </View>
                        ) : this.state.focusedInput == 2 ? (
                          <View style={{ width: "100%", marginTop: 10 }}>
                            <View style={{ flexDirection: "row" }}>
                              <View style={{ width: "50%", paddingBottom: 10 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("Arial")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Arial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Arial" }}>
                                        Arial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "Advertising_Script_Monoline_Trial"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "CourierNewPSMT" ||
                                        this.state.fontFamily2 ==
                                          "Advertising_Script_Monoline_Trial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "Advertising_Script_Monoline_Trial"
                                        }}
                                      >
                                        Advertising Script Monoline Trial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("Biloxi_Script")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Sharmeen" ||
                                        this.state.fontFamily2 ==
                                          "Biloxi_Script"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Biloxi_Script" }}
                                      >
                                        Biloxi Script
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("LateefRegOT")
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "Ulnoreenbld" ||
                                        this.state.fontFamily2 == "LateefRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LateefRegOT" }}
                                      >
                                        LateefRegOT
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("Calibri_Bold")
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "Urdu_Emad_Nastaliq" ||
                                        this.state.fontFamily2 == "Calibri_Bold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Calibri_Bold" }}
                                      >
                                        Calibri Bold
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "Confetti_Stream"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "Urdulife_italic" ||
                                        this.state.fontFamily2 ==
                                          "Confetti_Stream"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Confetti_Stream"
                                        }}
                                      >
                                        Confetti Stream
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("english_essay")
                                      }
                                      style={
                                        this.state.fontFamily2 == "UrduLink" ||
                                        this.state.fontFamily2 ==
                                          "english_essay"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "english_essay" }}
                                      >
                                        English essay
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("SinkinSans")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Zafar" ||
                                        this.state.fontFamily2 == "SinkinSans"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "SinkinSans" }}
                                      >
                                        Sinkin Sans
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("haldanor")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Zohra" ||
                                        this.state.fontFamily2 == "haldanor"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "haldanor" }}>
                                        Haldanor
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "alvi_Nastaleeq_Lahori_shipped"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                        "alvi_Nastaleeq_Lahori_shipped"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "alvi_Nastaleeq_Lahori_shipped"
                                        }}
                                      >
                                        Jameel Noori Nastaleeq
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                              <View style={{ width: "50%", paddingBottom: 10 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("LearningCurve")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Mishafi" ||
                                        this.state.fontFamily2 ==
                                          "LearningCurve"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LearningCurve" }}
                                      >
                                        LearningCurve
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "SohoGothicW04_Regular"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "Times New Roman" ||
                                        this.state.fontFamily2 ==
                                          "SohoGothicW04_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "SohoGothicW04_Regular"
                                        }}
                                      >
                                        Soho
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "ScheherazadeRegOT"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                        "ScheherazadeRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "ScheherazadeRegOT"
                                        }}
                                      >
                                        ScheherazadeRegOT
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("MintSpirit")
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "TimesNewRomanPSMT" ||
                                        this.state.fontFamily2 == "MintSpirit"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "MintSpirit" }}
                                      >
                                        Mint Spirit
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "Signika_Regular"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "DamascusLight" ||
                                        this.state.fontFamily2 ==
                                          "Signika_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Regular"
                                        }}
                                      >
                                        Signika Regular
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "Signika_Semibold"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "DamascusMedium" ||
                                        this.state.fontFamily2 ==
                                          "Signika_Semibold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Semibold"
                                        }}
                                      >
                                        Signika Semibold
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("Tahoma")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Tahoma"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Tahoma" }}>
                                        Tahoma
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2("Times")
                                      }
                                      style={
                                        this.state.fontFamily2 == "Farah" ||
                                        this.state.fontFamily2 == "Times"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Times" }}>
                                        Times
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily2(
                                          "TravelingTypewriter"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily2 ==
                                          "DamascusSemiBold" ||
                                        this.state.fontFamily2 ==
                                          "TravelingTypewriter"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "TravelingTypewriter"
                                        }}
                                      >
                                        Traveling Typewriter
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                            </View>
                          </View>
                        ) : this.state.focusedInput == 3 ? (
                          <View style={{ width: "100%", marginTop: 10 }}>
                            <View style={{ flexDirection: "row" }}>
                              <View style={{ width: "50%", paddingBottom: 0 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("Arial")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Arial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Arial" }}>
                                        Arial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "Advertising_Script_Monoline_Trial"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "CourierNewPSMT" ||
                                        this.state.fontFamily3 ==
                                          "Advertising_Script_Monoline_Trial"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "Advertising_Script_Monoline_Trial"
                                        }}
                                      >
                                        Advertising Script Monoline Trial
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("Biloxi_Script")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Sharmeen" ||
                                        this.state.fontFamily3 ==
                                          "Biloxi_Script"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Biloxi_Script" }}
                                      >
                                        Biloxi Script
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("LateefRegOT")
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "Ulnoreenbld" ||
                                        this.state.fontFamily3 == "LateefRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LateefRegOT" }}
                                      >
                                        LateefRegOT
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("Calibri_Bold")
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "Urdu_Emad_Nastaliq" ||
                                        this.state.fontFamily3 == "Calibri_Bold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "Calibri_Bold" }}
                                      >
                                        Calibri Bold
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "Confetti_Stream"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "Urdulife_italic" ||
                                        this.state.fontFamily3 ==
                                          "Confetti_Stream"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Confetti_Stream"
                                        }}
                                      >
                                        Confetti Stream
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("english_essay")
                                      }
                                      style={
                                        this.state.fontFamily3 == "UrduLink" ||
                                        this.state.fontFamily3 ==
                                          "english_essay"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "english_essay" }}
                                      >
                                        English essay
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("SinkinSans")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Zafar" ||
                                        this.state.fontFamily3 == "SinkinSans"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "SinkinSans" }}
                                      >
                                        Sinkin Sans
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("haldanor")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Zohra" ||
                                        this.state.fontFamily3 == "haldanor"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "haldanor" }}>
                                        Haldanor
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "alvi_Nastaleeq_Lahori_shipped"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                        "alvi_Nastaleeq_Lahori_shipped"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily:
                                            "alvi_Nastaleeq_Lahori_shipped"
                                        }}
                                      >
                                        Jameel Noori Nastaleeq
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                              <View style={{ width: "50%", paddingBottom: 0 }}>
                                <ScrollView>
                                  <List>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("LearningCurve")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Mishafi" ||
                                        this.state.fontFamily3 ==
                                          "LearningCurve"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "LearningCurve" }}
                                      >
                                        LearningCurve
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "SohoGothicW04_Regular"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "Times New Roman" ||
                                        this.state.fontFamily3 ==
                                          "SohoGothicW04_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "SohoGothicW04_Regular"
                                        }}
                                      >
                                        Soho
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "ScheherazadeRegOT"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                        "ScheherazadeRegOT"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "ScheherazadeRegOT"
                                        }}
                                      >
                                        ScheherazadeRegOT
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("MintSpirit")
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "TimesNewRomanPSMT" ||
                                        this.state.fontFamily3 == "MintSpirit"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{ fontFamily: "MintSpirit" }}
                                      >
                                        Mint Spirit
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "Signika_Regular"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "DamascusLight" ||
                                        this.state.fontFamily3 ==
                                          "Signika_Regular"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Regular"
                                        }}
                                      >
                                        Signika Regular
                                      </Text>
                                    </ListItem>

                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "Signika_Semibold"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "DamascusMedium" ||
                                        this.state.fontFamily3 ==
                                          "Signika_Semibold"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "Signika_Semibold"
                                        }}
                                      >
                                        Signika Semibold
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("Tahoma")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Tahoma"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Tahoma" }}>
                                        Tahoma
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3("Times")
                                      }
                                      style={
                                        this.state.fontFamily3 == "Farah" ||
                                        this.state.fontFamily3 == "Times"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text style={{ fontFamily: "Times" }}>
                                        Times
                                      </Text>
                                    </ListItem>
                                    <ListItem
                                      onPress={() =>
                                        this.changeFontFamily3(
                                          "TravelingTypewriter"
                                        )
                                      }
                                      style={
                                        this.state.fontFamily3 ==
                                          "DamascusSemiBold" ||
                                        this.state.fontFamily3 ==
                                          "TravelingTypewriter"
                                          ? {
                                              backgroundColor: "#A5B3BC",
                                              paddingLeft: 5
                                            }
                                          : null
                                      }
                                    >
                                      <Text
                                        style={{
                                          fontFamily: "TravelingTypewriter"
                                        }}
                                      >
                                        Traveling Typewriter
                                      </Text>
                                    </ListItem>
                                  </List>
                                </ScrollView>
                              </View>
                            </View>
                          </View>
                        ) : null
                      ) : null}
                      {this.state.tab == 3 ? (
                        this.state.focusedInput == 1 ? (
                          <View
                            style={{
                              width: "100%",
                              marginTop: 25,
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <View style={{ width: "50%" }}>
                              <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.decreaseFontsize}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: 80,
                                      borderWidth: 0.5,
                                      borderColor: "#ADD8E6",
                                      marginRight: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      -
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.increaseFontsize}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      borderWidth: 0.5,
                                      height: 80,
                                      width: 80,
                                      borderColor: "#ADD8E6",
                                      marginLeft: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      +
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  width: "100%",
                                  alignContent: "center",
                                  alignItems: "center",
                                  marginTop: 20
                                }}
                              >
                                <View style={{ width: "70%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <View style={{ paddingLeft: 20 }}>
                                      <Text style={{ fontSize: 18 }}>
                                        Outline
                                      </Text>
                                    </View>
                                    <CheckBox
                                      onPress={() => this.text1Outline()}
                                      checked={this.state.text1Outline}
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        ) : this.state.focusedInput == 2 ? (
                          <View
                            style={{
                              width: "100%",
                              marginTop: 25,
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <View style={{ width: "50%" }}>
                              <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.decreaseFontsize2}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: 80,
                                      borderWidth: 0.5,
                                      borderColor: "#ADD8E6",
                                      marginRight: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      -
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.increaseFontsize2}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      borderWidth: 0.5,
                                      height: 80,
                                      width: 80,
                                      borderColor: "#ADD8E6",
                                      marginLeft: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      +
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  width: "100%",
                                  alignContent: "center",
                                  alignItems: "center",
                                  marginTop: 20
                                }}
                              >
                                <View style={{ width: "70%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <View style={{ paddingLeft: 20 }}>
                                      <Text style={{ fontSize: 18 }}>
                                        Outline
                                      </Text>
                                    </View>
                                    <CheckBox
                                      onPress={() => this.text2Outline()}
                                      checked={this.state.text2Outline}
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        ) : this.state.focusedInput == 3 ? (
                          <View
                            style={{
                              width: "100%",
                              marginTop: 25,
                              alignContent: "center",
                              alignItems: "center"
                            }}
                          >
                            <View style={{ width: "50%" }}>
                              <View style={{ flexDirection: "row" }}>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.decreaseFontsize3}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: 80,
                                      borderWidth: 0.5,
                                      borderColor: "#ADD8E6",
                                      marginRight: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      -
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: 80 }}
                                  onPress={this.increaseFontsize3}
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      borderWidth: 0.5,
                                      height: 80,
                                      width: 80,
                                      borderColor: "#ADD8E6",
                                      marginLeft: 20,
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center"
                                    }}
                                  >
                                    <Text
                                      style={{
                                        fontSize: 44,
                                        fontWeight: "bold"
                                      }}
                                    >
                                      +
                                    </Text>
                                  </View>
                                </TouchableOpacity>
                              </View>
                              <View
                                style={{
                                  width: "100%",
                                  alignContent: "center",
                                  alignItems: "center",
                                  marginTop: 20
                                }}
                              >
                                <View style={{ width: "70%" }}>
                                  <View style={{ flexDirection: "row" }}>
                                    <View style={{ paddingLeft: 20 }}>
                                      <Text style={{ fontSize: 18 }}>
                                        Outline
                                      </Text>
                                    </View>
                                    <CheckBox
                                      onPress={() => this.text3Outline()}
                                      checked={this.state.text3Outline}
                                    />
                                  </View>
                                </View>
                              </View>
                            </View>
                          </View>
                        ) : null
                      ) : null}
                      {this.state.tab == 4 ? (
                        this.state.focusedInput == 1 ? (
                          <View
                            style={{
                              width: "100%",
                              height: 300,
                              paddingTop: 10,
                              paddingBottom: 60
                            }}
                          >
                            <ScrollView style={{ paddingBottom: 50 }}>
                              <View
                                style={{
                                  flexDirection: "row",
                                  flexWrap: "wrap"
                                }}
                              >
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff3300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff3300"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff3300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffffff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffffff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffffff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#000000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#000000"
                                    }}
                                  >
                                    {this.state.fontColor == "#000000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#009933")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009933"
                                    }}
                                  >
                                    {this.state.fontColor == "#009933" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#6600ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#6600ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#6600ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff9900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff9900"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff9900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#663300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#663300"
                                    }}
                                  >
                                    {this.state.fontColor == "#663300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffff80")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffff80"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffff80" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff99ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff99ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff99ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#b3ccff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#b3ccff"
                                    }}
                                  >
                                    {this.state.fontColor == "#b3ccff" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ccffcc")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ccffcc"
                                    }}
                                  >
                                    {this.state.fontColor == "#ccffcc" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff0000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff0000"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff0000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#808080")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#808080"
                                    }}
                                  >
                                    {this.state.fontColor == "#808080" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#e6e6e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6e6e6"
                                    }}
                                  >
                                    {this.state.fontColor == "#e6e6e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#f2f2f2")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#f2f2f2"
                                    }}
                                  >
                                    {this.state.fontColor == "#f2f2f2" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#e6eeff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6eeff"
                                    }}
                                  >
                                    {this.state.fontColor == "#e6eeff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#993300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#993300"
                                    }}
                                  >
                                    {this.state.fontColor == "#993300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#990099")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#990099"
                                    }}
                                  >
                                    {this.state.fontColor == "#990099" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#0066ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#0066ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#0066ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#e6f0ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6f0ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#e6f0ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#003d99")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#003d99"
                                    }}
                                  >
                                    {this.state.fontColor == "#003d99" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#66a3ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#66a3ff"
                                    }}
                                  >
                                    {this.state.fontColor == "#66a3ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#00ff00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#00ff00"
                                    }}
                                  >
                                    {this.state.fontColor == "#00ff00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#009900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009900"
                                    }}
                                  >
                                    {this.state.fontColor == "#009900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#006600")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#006600"
                                    }}
                                  >
                                    {this.state.fontColor == "#006600" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#4d4d4d")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#4d4d4d"
                                    }}
                                  >
                                    {this.state.fontColor == "#4d4d4d" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#fff5e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#fff5e6"
                                    }}
                                  >
                                    {this.state.fontColor == "#fff5e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#ffc266")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffc266"
                                    }}
                                  >
                                    {this.state.fontColor == "#ffc266" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor("#995c00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#995c00"
                                    }}
                                  >
                                    {this.state.fontColor == "#995c00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                              </View>
                            </ScrollView>
                          </View>
                        ) : this.state.focusedInput == 2 ? (
                          <View
                            style={{
                              width: "100%",
                              height: 300,
                              paddingTop: 10,
                              paddingBottom: 60
                            }}
                          >
                            <ScrollView style={{ paddingBottom: 50 }}>
                              <View
                                style={{
                                  flexDirection: "row",
                                  flexWrap: "wrap"
                                }}
                              >
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff3300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff3300"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff3300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffffff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffffff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffffff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#000000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#000000"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#000000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#009933")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009933"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#009933" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#6600ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#6600ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#6600ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff9900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff9900"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff9900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#663300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#663300"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#663300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffff80")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffff80"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffff80" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff99ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff99ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff99ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#b3ccff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#b3ccff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#b3ccff" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ccffcc")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ccffcc"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ccffcc" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff0000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff0000"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff0000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#808080")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#808080"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#808080" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#e6e6e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6e6e6"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#e6e6e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#f2f2f2")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#f2f2f2"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#f2f2f2" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#e6eeff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6eeff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#e6eeff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#993300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#993300"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#993300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#990099")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#990099"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#990099" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#0066ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#0066ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#0066ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#e6f0ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6f0ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#e6f0ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#003d99")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#003d99"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#003d99" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#66a3ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#66a3ff"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#66a3ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#00ff00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#00ff00"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#00ff00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#009900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009900"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#009900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#006600")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#006600"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#006600" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#4d4d4d")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#4d4d4d"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#4d4d4d" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#fff5e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#fff5e6"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#fff5e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#ffc266")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffc266"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#ffc266" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor2("#995c00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#995c00"
                                    }}
                                  >
                                    {this.state.fontColor2 == "#995c00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                              </View>
                            </ScrollView>
                          </View>
                        ) : this.state.focusedInput == 3 ? (
                          <View
                            style={{
                              width: "100%",
                              height: 300,
                              paddingTop: 10,
                              paddingBottom: 60
                            }}
                          >
                            <ScrollView style={{ paddingBottom: 50 }}>
                              <View
                                style={{
                                  flexDirection: "row",
                                  flexWrap: "wrap"
                                }}
                              >
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff3300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff3300"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff3300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffffff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffffff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffffff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#000000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#000000"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#000000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#009933")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009933"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#009933" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#6600ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#6600ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#6600ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff9900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff9900"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff9900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#663300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#663300"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#663300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffff80")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffff80"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffff80" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff99ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff99ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff99ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#b3ccff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#b3ccff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#b3ccff" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ccffcc")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ccffcc"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ccffcc" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff0000")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff0000"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff0000" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#808080")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#808080"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#808080" ? (
                                      <Icon
                                        style={{ color: "#000" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#e6e6e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6e6e6"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#e6e6e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#f2f2f2")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#f2f2f2"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#f2f2f2" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#e6eeff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6eeff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#e6eeff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#993300")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#993300"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#993300" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffe6ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffe6ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffe6ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#990099")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#990099"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#990099" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ff00ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ff00ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ff00ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#0066ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#0066ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#0066ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#e6f0ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6f0ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#e6f0ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#003d99")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#003d99"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#003d99" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#66a3ff")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#66a3ff"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#66a3ff" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#e6ffe6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#e6ffe6"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#e6ffe6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#00ff00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#00ff00"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#00ff00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#009900")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#009900"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#009900" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#006600")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#006600"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#006600" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#4d4d4d")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#4d4d4d"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#4d4d4d" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#fff5e6")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#fff5e6"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#fff5e6" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#ffc266")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#ffc266"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#ffc266" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                  style={{ height: 80, width: "25%" }}
                                  onPress={() =>
                                    this.changeFontColor3("#995c00")
                                  }
                                >
                                  <View
                                    style={{
                                      paddingHorizontal: 10,
                                      height: 80,
                                      width: "100%",
                                      alignItems: "center",
                                      alignContent: "center",
                                      justifyContent: "center",
                                      backgroundColor: "#995c00"
                                    }}
                                  >
                                    {this.state.fontColor3 == "#995c00" ? (
                                      <Icon
                                        style={{ color: "#808080" }}
                                        name="checkmark-circle"
                                      />
                                    ) : null}
                                  </View>
                                </TouchableOpacity>
                              </View>
                            </ScrollView>
                          </View>
                        ) : null
                      ) : null}
                    </View>
                  ) : null}
                </ViewShot>
              </View>
            </ImageBackground>
          </ViewShot>
        )}
        <ActionSheet
          ref={o => (this.ActionSheet = o)}
          options={["Save to Photos", "Share", "Delete", "Settings", "Cancel"]}
          cancelButtonIndex={4}
          destructiveButtonIndex={4}
          onPress={index => {
            if (index == 0) {
              this.captureImage();
            }
            if (index == 1) {
              this.onShare();
            }
            if (index == 2) {
              this.setState({
                message: "",
                fontSize: 22,
                fontColor: "black",
                fontFamily: "Arial",
                message2: "",
                fontSize2: 22,
                fontColor2: "black",
                fontFamily2: "Arial",
                message3: "",
                fontSize3: 22,
                fontColor3: "black",
                fontFamily3: "Arial",
                backgroundColor: "transparent",
                bgImageSize: "cover",
                first_x : 10,
                first_y : 10,
                 second_x : 10,
                second_y : 70,
                 third_x : 10,
                third_y : 130,
                image:
                  "https://backgrounddownload.com/wp-content/uploads/2018/09/1x1-white-background-3.jpg",
                appBackground: 0,
                text1: true,
                text2: false,
                text3: false
              });
            }
            if (index == 3) {
              navigation.navigate("Settings");
            }
          }}
        />
        <ActionSheet
          ref={o => (this.TextActionSheet = o)}
          options={["Delete", "Cancel"]}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={index => {

            if (index == 0) {
              if (this.state.text2 == false && this.state.text3 == false) {
                alert("No more Text fields to delete");
              } else {
                this.setState({
                  message: "",
                  fontSize: 22,
                  fontColor: "black",
                  fontFamily: "Arial",
                  text1: false,
                  showtab: false
                });
              }
            }
            if (index == 1) {
              this.textInputField.blur();
              Keyboard.dismiss();
            }
          }}
        />
        <ActionSheet
          ref={o => (this.Text2ActionSheet = o)}
          options={["Delete", "Cancel"]}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={index => {

            if (index == 0) {
              if (this.state.text1 == false && this.state.text3 == false) {
                alert("No more Text fields to delete");
              } else {
                this.setState({
                  message2: "",
                  fontSize2: 22,
                  fontColor2: "black",
                  fontFamily2: "Arial",
                  text2: false,
                  showtab: false
                });
              }
            }
            if (index == 1) {
              this.text2InputField.blur();
              Keyboard.dismiss();
            }
          }}
        />
        <ActionSheet
          ref={o => (this.Text3ActionSheet = o)}
          options={["Delete", "Cancel"]}
          cancelButtonIndex={1}
          destructiveButtonIndex={1}
          onPress={index => {
            if (index == 0) {
              if (this.state.text1 == false && this.state.text2 == false) {
                alert("No more Text fields to delete");
              } else {
                this.setState({
                  message3: "",
                  fontSize3: 22,
                  fontColor3: "black",
                  fontFamily3: "Arial",
                  text3: false,
                  showtab: false
                });
              }
            }
            if (index == 1) {
              this.text3InputField.blur();
              Keyboard.dismiss();
            }
          }}
        />
      </Container>
    );
  }
}
function elevationShadowStyle(elevation) {
  return {
    elevation,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0.5 * elevation },
    shadowOpacity: 0.3,
    shadowRadius: 0.8 * elevation
  };
}
const styles = StyleSheet.create({
  shadow1: elevationShadowStyle(5),
  shadow2: elevationShadowStyle(10),
  shadow3: elevationShadowStyle(20),
  Bigbox: {
    width: "90%",
    height: deviceHeight / 3,
    borderRadius: 8,
    backgroundColor: "white",
    marginTop: 30
  },
  box: {
    width: "45%",
    height: deviceHeight / 6,
    borderRadius: 8,
    backgroundColor: "white"
  },
  footer: {
    width: deviceWidth,
    height: 40,
    marginTop: "auto",
    borderTopWidth: 0.5,
    borderTopColor: "#808080"
  },
  shadedFooter: {
    width: deviceWidth,
    height: 40,
    marginTop: "auto",
    borderRadius: 8,
    backgroundColor: "white"
  },
  container: {
    flex: 1,
    backgroundColor: "#ecf0f1",
    padding: 24
  },
  centerContent: {
    justifyContent: "space-around",
    alignItems: "center"
  },
  ball: {
    borderRadius: 128,
    width: 128,
    height: 128,
    backgroundColor: "lightblue"
  }
});
