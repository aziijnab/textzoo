//import React from 'react';
import React, { Component } from "react";
import { Dimensions, View } from "react-native";
import {
  Container,
  Content,
  Button,
  ListItem,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Switch
} from "native-base";
import BaseHeader from "../../components/BaseHeader";

var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class HowToUse extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      username_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null
    };
    this.navigate = this.props.navigation.navigate;
  }

  render() {
    return (
      <Container>
        <BaseHeader
          back={true}
          navigation={this.props.navigation}
          title={"How To Use App"}
        />
        <Content>
          <View
            style={{
              width: deviceWidth,
              height: deviceHeight,
              alignItems: "center",
              alignContent: "center"
            }}
          >
            <View style={{ width: "90%", marginTop: 50 }}>
              <Text style={{ textAlign: "left" }}>
                You can choose background color or image and write text on it
                with different colors sizes and fonts. Below is each icon and
                its purpose:
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                1. From the left 1st icon to add image or background color
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                2. From left 2nd font icon is to select font type typewriter
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                3. From left 3rd size icon is to change text size size of your
                choice
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                4. Color icon is to change color of text
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                5. + icon is for adding different color text
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                6. Keyboard icon to show keyboard and typewrite
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                7. Check mark icon is to save to photos or share on social media
              </Text>
              <Text style={{ paddingVertical: 2 }}></Text>
              <Text style={{ textAlign: "left" }}>
                8. Down arrow icon is to move toolbar at bottom
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

export default HowToUse;
