//import React from 'react';
import React, { Component } from "react";
import { Dimensions, View } from "react-native";
import {
  Container,
  Content,
  Button,
  ListItem,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Switch
} from "native-base";
import BaseHeader from "../../components/BaseHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Copyrights extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      username_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null
    };
    this.navigate = this.props.navigation.navigate;
  }

  render() {
    return (
      <Container>
        <BaseHeader
          back={true}
          navigation={this.props.navigation}
          title={"Copyrights"}
        />
        <Content>
          <View
            style={{
              width: deviceWidth,
              height: deviceHeight,
              alignItems: "center",
              alignContent: "center"
            }}
          >
            <View style={{ width: "90%", marginTop: 50 }}>
              <Text style={{ alignItems: "center", alignContent: "center" }}>
                All rights reserved. No part of this App may be reproduced in
                any form or by any means, including photocopying. recording, or
                other electronic or mechanical methods without the prior written
                permission of the publisher, except in the case of brief
                quotations embodied in critical reviews and certain other
                noncommercial uses permitted by copyright law. For permission
                requests, write to the publisher.
              </Text>
            </View>
          </View>
        </Content>
      </Container>
    );
  }
}

export default Copyrights;
