//import React from 'react';
import React, { Component } from "react";
import { Dimensions, TouchableOpacity, Linking, Clipboard } from "react-native";
import {
  Container,
  Content,
  Button,
  ListItem,
  Text,
  Icon,
  Left,
  Body,
  Right,
  Switch
} from "native-base";
import BaseHeader from "../../components/BaseHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      username_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null
    };
    this.navigate = this.props.navigation.navigate;
  }
  writeToClipboard = async () => {
    Clipboard.setString(
      "https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
    );
    alert("Copied to Clipboard!");
  };
  render() {
    return (
      <Container>
        <BaseHeader
          back={true}
          navigation={this.props.navigation}
          title={"Settings"}
        />
        <Content>
          <ListItem icon>
            <Left>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("HowToUse")}
              >
                <Button style={{ backgroundColor: "#FF9501" }}>
                  <Icon active name="ios-bulb" />
                </Button>
              </TouchableOpacity>
            </Left>
            <Body>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("HowToUse")}
              >
                <Text>How to Use App</Text>
              </TouchableOpacity>
            </Body>

            <Right>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("HowToUse")}
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:?subject=Invitation&body=https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
                  )
                }
              >
                <Button style={{ backgroundColor: "#007AFF" }}>
                  <Icon active name="mail" />
                </Button>
              </TouchableOpacity>
            </Left>

            <Body>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:?subject=Invitation&body=https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
                  )
                }
              >
                <Text>Email App Link to Friend</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:?subject=Invitation&body=https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
                  )
                }
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:?subject=Invitation&body=https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
                  )
                }
              >
                <Button style={{ backgroundColor: "#007AFF" }}>
                  <Icon active name="mail" />
                </Button>
              </TouchableOpacity>
            </Left>
            <Body>
              <TouchableOpacity onPress={() => this.writeToClipboard()}>
                <Text>Copy App link to share</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:?subject=Invitation&body=https://play.google.com/store/apps/details?id=com.dmtc.textzoo"
                  )
                }
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:support@textzoo.com?subject=Invitation&body="
                  )
                }
              >
                <Button style={{ backgroundColor: "#007AFF" }}>
                  <Icon active name="mail" />
                </Button>
              </TouchableOpacity>
            </Left>
            <Body>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:digitmatic@yahoo.com?subject=Feedback&title=Feedback&body="
                  )
                }
              >
                <Text>Feedback for more Features</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() =>
                  Linking.openURL(
                    "mailto:digitmatic@yahoo.com?subject=Feedback&title=Feedback&body="
                  )
                }
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="paper" />
              </Button>
            </Left>
            <Body>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("PrivacyPolicy")}
              >
                <Text>Privacy Policy</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("PrivacyPolicy")}
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="paper" />
              </Button>
            </Left>
            <Body>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Copyrights")}
              >
                <Text>Copyright</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("Copyrights")}
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
          <ListItem icon>
            <Left>
              <Button style={{ backgroundColor: "#007AFF" }}>
                <Icon active name="paper" />
              </Button>
            </Left>
            <Body>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("TermsOfUse")}
              >
                <Text>Terms of Use</Text>
              </TouchableOpacity>
            </Body>
            <Right>
              <TouchableOpacity
                onPress={() => this.props.navigation.navigate("TermsOfUse")}
              >
                <Icon active name="arrow-forward" />
              </TouchableOpacity>
            </Right>
          </ListItem>
        </Content>
      </Container>
    );
  }
}

export default Settings;
