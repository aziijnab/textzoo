//import React from 'react';
import React, { Component } from "react";
import {
  Dimensions,
  View
} from "react-native";
import { Container, Content, Button, ListItem, Text, Icon, Left, Body, Right, Switch } from 'native-base';
import BaseHeader from "../../components/BaseHeader";
var deviceHeight = Dimensions.get("window").height;
var deviceWidth = Dimensions.get("window").width;

class PrivacyPolicy extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      username_err: "",
      password_err: "",
      isLoading: "",
      userInfo: null,
    };
    this.navigate = this.props.navigation.navigate;
  }
 
  render() {
    return (
     <Container>
 <BaseHeader back={true} navigation={this.props.navigation} title={"Privacy Policy"} />
 <Content>
          <View style={{width:deviceWidth,height:deviceHeight, alignItems:"center", alignContent:"center"}}>
              <View style={{width:"90%", marginTop:50}}>
                  <Text style={{alignItems:"center", alignContent:"center"}}>
                  It is our policy not to share any of your information entered
with us with anyone, this includes 3rd party sites
advertisers, and etc
                  </Text>
              </View>
          </View>
        </Content></Container>
    );
  }
}



export default PrivacyPolicy;
