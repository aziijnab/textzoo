import { createAppContainer, createSwitchNavigator, createStackNavigator } from 'react-navigation';

import Home from '../screens/Home';
import Settings from "../screens/Settings";
import HowToUse from "../screens/Settings/HowToUse";
import PrivacyPolicy from "../screens/Settings/PrivacyPolicy";
import TermsOfUse from "../screens/Settings/TermsOfUse";
import Copyrights from "../screens/Settings/Copyrights";

export default createAppContainer(
  createStackNavigator({
    Home: { screen: Home },
    Settings: { screen: Settings },
    HowToUse: { screen: HowToUse },
    PrivacyPolicy: { screen: PrivacyPolicy },
    TermsOfUse: { screen: TermsOfUse },
    Copyrights: { screen: Copyrights },
  },
  {
    headerMode: "none"
  })
);
